import sys

# Load external packages
import configparser
from PyQt5 import QtWidgets

# Load coded packages
from UserInterface.back_end_worker import Worker
from UserInterface.display import Dialog


if __name__ == "__main__":
    # Read config file
    # Note: The full path is needed for autostart (which occurs in a different folder)
    ini_file_name = "config.ini"

    parser = configparser.ConfigParser()
    parser.read(ini_file_name)

    # Create backend
    back_end = Worker(parser)

    # Create the Qt Application. Must be done before any Qt object is created.
    app = QtWidgets.QApplication(sys.argv)

    # Create frontend and pass in backend to link up
    front_end = Dialog(parser, back_end)

    # Run application
    front_end.show()
    sys.exit(app.exec_())
