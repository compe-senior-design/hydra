import adafruit_lsm303dlh_mag
from numpy import arctan2
from numpy import pi
from SensorModules.I2CSensor import I2CSensor
from SensorModules.States import State


# LSM303 Sensor
class Compass(I2CSensor):

    def __init__(self, X_OFFSET, Y_OFFSET, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        super(Compass, self).__init__(FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG)
        self.sensor = "LSM303"
        self.mag = None
        self.current_direction_deg = None
        self.current_direction_cardinal = None
        self.x_offset = X_OFFSET
        self.y_offset = Y_OFFSET
        self.sensor_data = {"Sensor": self.sensor,
                            "State": State.DISCONNECTED,
                            "Data": {
                                "Degree Direction": self.current_direction_deg,
                                "Cardinal Direction": self.current_direction_cardinal
                            }
                            }

        self.write_data(self.sensor_data)

    def read_from_sensor(self):

        try:
            mag_read = self.mag.magnetic
        except Exception as e:
            self.logger.warning("%s: Failed trying read data from sensor.", self.sensor)
            raise e

        # Set the direction
        self._set_direction(mag_read)

    def update_data(self):
        self.read_from_sensor()

        self.sensor_data["Data"]["Degree Direction"] = "{0:.0f}".format(self.current_direction_deg)
        self.sensor_data["Data"]["Cardinal Direction"] = self.current_direction_cardinal

        self.write_data(self.sensor_data)

    def disconnected(self):
        # Open connection via I2C
        self.connect_via_i2c()

        # Clear out real-time data
        self.sensor_data["Data"]["Degree Direction"] = None
        self.sensor_data["Data"]["Cardinal Direction"] = None
        self.write_data(self.sensor_data)

        try:
            # Attempt connection to sensor
            self.mag = adafruit_lsm303dlh_mag.LSM303DLH_Mag(self.i2c)
        except Exception as e:
            self.logger.warning("%s: Failed trying to connected to sensor.", self.sensor)
            raise e

        # Connection acquired
        self.logger.debug("%s: Connection acquired. ", self.sensor)
        self.update_state(State.INITIALIZE)

    def _set_direction(self, mag_read):
        x = mag_read[0] + self.x_offset
        y = mag_read[1] + self.y_offset

        angle = arctan2(y, x) * 180/pi

        # Convert the angle to a normalized 0-360 degree compass direction
        direction_deg = angle
        if angle < 0:
            direction_deg += 360

        self.current_direction_deg = direction_deg

        # Convert the angle to a cardinal direction
        direction_cardinal = "N/A"

        if -22.5 < angle <= 22.5:
            direction_cardinal = "N"
        elif 22.5 < angle <= 67.5:
            direction_cardinal = "NE"
        elif 67.5 < angle <= 112.5:
            direction_cardinal = "E"
        elif 112.5 < angle <= 157.5:
            direction_cardinal = "SE"
        elif angle > 157.5 or angle <= -157.5:
            direction_cardinal = "S"
        elif -157.5 < angle <= -112.5:
            direction_cardinal = "SW"
        elif -112.5 < angle <= -67.5:
            direction_cardinal = "W"
        elif -67.5 < angle <= -22.5:
            direction_cardinal = "NW"

        self.current_direction_cardinal = direction_cardinal
