import adafruit_mcp9808
from SensorModules.I2CSensor import I2CSensor
from SensorModules.States import State
from Utility.sensor_utilities import convert_c_to_f


# MCP9808 Sensor
class AmbientTemp(I2CSensor):

    def __init__(self, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        super(AmbientTemp, self).__init__(FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG)
        self.sensor = "MCP9808"
        self.mcp = None
        self.current_temp = None
        self.high_temp = None
        self.low_temp = None

        self.sensor_data = {"Sensor": self.sensor,
                            "State": State.DISCONNECTED,
                            "Data": {
                                "Current Temperature": self.current_temp,
                                "High Temp": self.high_temp,
                                "Low Temp": self.low_temp
                            }
                            }

        self.write_data(self.sensor_data)

    def read_from_sensor(self):

        try:
            temp = self.mcp.temperature
        except Exception as e:
            self.logger.warning("%s: Failed trying read data from sensor.", self.sensor)
            raise e

        # Set the temperatures
        self._set_temps(temp)

    def update_data(self):
        self.read_from_sensor()

        # Format and convert to Fahrenheit
        self.sensor_data["Data"]["Current Temperature"] = "{:.0f}".format(convert_c_to_f(self.current_temp))
        self.sensor_data["Data"]["High Temp"] = "{:.0f}".format(convert_c_to_f(self.high_temp))
        self.sensor_data["Data"]["Low Temp"] = "{:.0f}".format(convert_c_to_f(self.low_temp))

        self.write_data(self.sensor_data)

    def disconnected(self):
        # Open connection via I2C
        self.connect_via_i2c()

        # Clear out real-time data
        self.sensor_data["Data"]["Current Temperature"] = None
        self.write_data(self.sensor_data)

        try:
            # Attempt connection to sensor
            self.mcp = adafruit_mcp9808.MCP9808(self.i2c)
        except Exception as e:
            self.logger.warning("%s: Failed trying to connected to sensor.", self.sensor)
            raise e

        # Connection acquired
        self.logger.debug("%s: Connection acquired.", self.sensor)
        self.update_state(State.INITIALIZE)

    def _set_temps(self, temp):

        # Set current temp
        self.current_temp = temp

        # Set high and low temps
        if not self.high_temp or self.current_temp > self.high_temp:
            self.high_temp = self.current_temp
        if not self.low_temp or self.current_temp < self.low_temp:
            self.low_temp = self.current_temp
