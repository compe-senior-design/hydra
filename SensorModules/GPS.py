import adafruit_gps
import serial
from SensorModules.BaseModule import BaseModule
from SensorModules.States import State
from Utility.sensor_utilities import convert_knots_to_mph, convert_utc_to_est, lat_long_distance
import datetime


# GPS Sensor
class GPS(BaseModule):

    def __init__(self, DIS_TRAVELED_NUM_WAITS, UPDATE_RETRIES_LIMIT, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        super(GPS, self).__init__(FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG)
        self.sensor = "GPS"
        self.uart = None
        self.gps = None
        self.latitude = None
        self.longitude = None
        self.old_latitude = None
        self.old_longitude = None
        self.speed = None
        self.distance_traveled = 0
        self.time = None
        self.max_speed = None
        self.uart_setup = False
        self.distance_traveled_num_waits = DIS_TRAVELED_NUM_WAITS
        self.count_waits = 0
        self.update_retries = 0
        self.update_retries_limit = UPDATE_RETRIES_LIMIT

        self.sensor_data = {"Sensor": self.sensor,
                            "State": State.DISCONNECTED,
                            "Data": {
                                "Latitude": self.latitude,
                                "Longitude": self.longitude,
                                "Speed": self.speed,
                                "Distance Traveled": self.distance_traveled,
                                "Time": self.time,
                                "Max Speed": self.max_speed,
                            }
                            }

        self.write_data(self.sensor_data)

    def _connect_via_uart(self):

        try:
            # Opens connection via UART
            self.uart = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=10)
        except Exception as e:
            self.logger.warning("%s: Failed to open connection via UART.", self.sensor)
            raise e

    def _attempt_gps_update(self):
        # Attempt to check for newly parsed data
        # If there is none, assume the GPS is disconnected
        if not self.gps.update():
            self.update_retries += 1
            if self.update_retries >= self.update_retries_limit:
                raise Exception('GPS unable to parse new data.')
        else:
            self.update_retries = 0
            new_data = True
            # Clear the buffer
            while new_data:
                new_data = self.gps.update()

    def read_from_sensor(self):
        # Make sure to call gps.update() every loop iteration and at least twice
        # as fast as data comes from the GPS unit (usually every second).
        # This returns a bool that's true if it parsed new data (you can ignore it
        # though if you don't care and instead look at the has_fix property).

        try:
            self._attempt_gps_update()
        except Exception as e:
            self.logger.warning("%s: Unable to receive data from GPS sensor", self.sensor)
            raise e

        self.latitude = self.gps.latitude
        self.longitude = self.gps.longitude

        self.time = self.gps.timestamp_utc

        # Some attributes beyond latitude, longitude and timestamp are optional
        # and might not be present.  Check if they're None before trying to use!
        if self.gps.speed_knots is not None:
            speed = self.gps.speed_knots
            self._set_speed(speed)
        else:
            self._set_speed(None)

    def update_data(self):
        self.read_from_sensor()

        self.sensor_data["Data"]["Latitude"] = "{0:.5f}".format(self.latitude)
        self.sensor_data["Data"]["Longitude"] = "{0:.5f}".format(self.longitude)

        if self.count_waits == 0:
            self.old_longitude = self.longitude
            self.old_latitude = self.latitude

        if self.count_waits == self.distance_traveled_num_waits:
            new_distance = lat_long_distance((self.old_latitude, self.old_longitude), (self.latitude, self.longitude))
            self.distance_traveled += new_distance
            self.sensor_data["Data"]["Distance Traveled"] = "{0:.2f}".format(self.distance_traveled)
            self.count_waits = 0
        else:
            # Increment waits
            self.count_waits += 1


        self.sensor_data["Data"]["Speed"] = "{0:.0f}".format(convert_knots_to_mph(self.speed))
        self.sensor_data["Data"]["Time"] = convert_utc_to_est(self.time)
        self.sensor_data["Data"]["Max Speed"] = "{0:.0f}".format(convert_knots_to_mph(self.max_speed))

        self.write_data(self.sensor_data)

    def disconnected(self):
        # We really only want to setup the UART connection once, otherwise it causes errors
        if not self.uart_setup:
            # Open connection via UART
            self._connect_via_uart()

            try:
                # Attempt connection to sensor
                self.gps = adafruit_gps.GPS(self.uart, debug=False)
            except Exception as e:
                self.logger.warning("%s: Failed trying to connected to sensor.", self.sensor)
                raise e

            self.uart_setup = True

        # Clear out real-time data
        self.sensor_data["Data"]["Latitude"] = None
        self.sensor_data["Data"]["Longitude"] = None
        self.sensor_data["Data"]["Speed"] = None
        self.sensor_data["Data"]["Time"] = None

        self.write_data(self.sensor_data)

        # Turn on the basic GGA and RMC info (what you typically want)
        self.gps.send_command(b"PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
        # Set update rate to once a second (1hz) which is what you typically want.
        self.gps.send_command(b"PMTK220,1000")

        # Finally, call the GPS update method to see if data is being passed over the line
        try:
            self._attempt_gps_update()
        except Exception as e:
            # We aren't connected yet, stay in this state
            self.logger.warning("%s: Unable to receive data from GPS sensor", self.sensor)
            raise e

        # Connection acquired
        self.logger.debug("%s: Connection acquired.", self.sensor)
        self.update_state(State.INITIALIZE)

    def connected(self):
        try:
            self.update_data()
        except Exception as e:
            self.logger.warning("%s: Failed trying to update data.", self.sensor)
            self.update_state(State.DISCONNECTED)
            raise e

    def initialize(self):
        # First, we need to get a GPS update
        try:
            self._attempt_gps_update()
        except Exception as e:
            self.logger.warning("%s: Unable to receive data from GPS sensor", self.sensor)
            self.update_state(State.DISCONNECTED)
            raise e

        # Then, we need to see if the GPS has a fix yet
        if not self.gps.has_fix:
            # If there is no fix, print out a debug message and stay in this state
            self.logger.debug("%s: Waiting for fix...", self.sensor)
        else:
            # If a fix has been acquired, try a sensor read
            try:
                self.read_from_sensor()
            except Exception as e:
                self.logger.warning("%s: Failed to read sensor data in initialize state.", self.sensor)
                self.update_state(State.DISCONNECTED)
                raise e

            # Successfully received data from sensor
            self.logger.debug("%s: Successfully received initial data.", self.sensor)
            self.update_state(State.CONNECTED)

    def _set_speed(self, speed):
        # Set current speed
        self.speed = speed

        # Set max speed
        if not self.max_speed or self.speed > self.max_speed:
            self.max_speed = self.speed
