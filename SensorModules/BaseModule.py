import logging
import time
import threading
from SensorModules.States import State


class BaseModule():

    def __init__(self, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        self.sensor = None

        # Protected variable! Don't interact without using read_data and write_data
        self._data = {"Sensor": self.sensor,
                     "State": State.DISCONNECTED,
                     "Data": {}}

        self.sensor_data = None
        self.FREQ_OF_DATA_READS = float(FREQ_OF_DATA_READS / 1000)
        self.FREQ_OF_TRYING_TO_CONNECT = float(FREQ_OF_TRYING_TO_CONNECT / 1000)
        self.DEBUG = DEBUG

        # Creating lock for data access
        self.data_lock = threading.Lock()

        # Create logging
        # Note: The full path is needed for autostart (which occurs in a different folder)
        logging.basicConfig(filename='sensor.log', filemode='w', format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger('sensor_logger')
        if DEBUG:
            self.logger.setLevel(logging.DEBUG)

    def read_data(self):
        # Returns data dict of the sensor

        self.data_lock.acquire()
        try:
            data = self._data
        finally:
            self.data_lock.release()

        return data

    def write_data(self, sensor_data):
        # Writes to the data dict of the sensor

        self.data_lock.acquire()
        try:
            self._data = sensor_data
        finally:
            self.data_lock.release()

    def update_state(self, state):
        # Updates the state
        self.sensor_data["State"] = state
        self.write_data(self.sensor_data)

    def run(self):
        # Main loop for each sensor module

        while True:
            try:
                if self.sensor_data["State"] == State.DISCONNECTED:
                    time.sleep(self.FREQ_OF_TRYING_TO_CONNECT)
                    self.disconnected()
                elif self.sensor_data["State"] == State.INITIALIZE:
                    time.sleep(self.FREQ_OF_DATA_READS)
                    self.initialize()
                elif self.sensor_data["State"] == State.CONNECTED:
                    time.sleep(self.FREQ_OF_DATA_READS)
                    self.connected()
                else:
                    self.sensor_data["State"] = State.DISCONNECTED
                    self.write_data(self.sensor_data)
                    raise Exception("Incorrect State assignment: " + str(self.sensor_data["State"]))
            except Exception as e:
                self.logger.error(e)

    # abstract methods
    def read_from_sensor(self):
        pass

    def update_data(self):
        # Updates data dict
        pass

    def disconnected(self):
        # Disconnected logic
        pass

    def connected(self):
        # Connected logic
        pass

    def initialize(self):
        # Initialize logic
        pass

    def dump_debug_info(self):
        pass
