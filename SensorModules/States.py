from enum import Enum


# Enumeration members for States
class State(Enum):
    DISCONNECTED = "Disconnected"
    INITIALIZE = "Initialize"
    CONNECTED = "Connected"
