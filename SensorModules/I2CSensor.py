import board
import busio
from SensorModules.BaseModule import BaseModule
from SensorModules.States import State


class I2CSensor(BaseModule):

    def __init__(self, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        super(I2CSensor, self).__init__(FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG)
        self.i2c = None

    def initialize(self):
        try:
            self.read_from_sensor()
        except Exception as e:
            self.logger.warning("%s: Failed to read sensor data in initialize state.", self.sensor)
            self.update_state(State.DISCONNECTED)
            raise e

        # Successfully received data from sensor
        self.logger.debug("%s: Successfully received initial data from sensor.", self.sensor)
        self.update_state(State.CONNECTED)

    def connect_via_i2c(self):
        # Opens connection via I2C
        try:
            self.i2c = busio.I2C(board.SCL, board.SDA)
        except Exception as e:
            self.logger.warning("%s: Failed trying to open connection via I2C.", self.sensor)
            raise e

    def connected(self):
        try:
            self.update_data()
        except Exception as e:
            self.logger.warning("%s: Failed trying to update data.", self.sensor)
            self.update_state(State.DISCONNECTED)
            raise e
