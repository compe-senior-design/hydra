import adafruit_bno055
from SensorModules.I2CSensor import I2CSensor
from SensorModules.States import State
from Utility.sensor_utilities import convert_linear_accl_to_gforce


# BNO055 Sensor
class Gyroscope(I2CSensor):

    def __init__(self, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        super(Gyroscope, self).__init__(FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG)
        self.sensor = "BNO055"
        self.bno = None
        self.pitch = None
        self.roll = None
        self.yaw = None
        self.gforce = None
        self.max_gforce = None
        self.max_degree_of_incline = None
        self.min_degree_of_incline = None

        self.sensor_data = {"Sensor": self.sensor,
                            "State": State.DISCONNECTED,
                            "Data": {
                                "Pitch": self.pitch,
                                "Roll": self.roll,
                                "Yaw": self.yaw,
                                "G-Force": self.gforce,
                                "Max G-Force": self.max_gforce,
                                "Max Degree of Incline": self.max_degree_of_incline,
                                "Min Degree of Incline": self.min_degree_of_incline
                            }
                            }

        self.write_data(self.sensor_data)

    def read_from_sensor(self):

        try:
            linear_acceleration = self.bno.linear_acceleration
            euler_angle = self.bno.euler
        except Exception as e:
            self.logger.warning("%s: Failed trying read data from sensor.", self.sensor)
            raise e

        # Set the all the values
        self._set_values(linear_acceleration, euler_angle)

    def update_data(self):
        self.read_from_sensor()

        self.sensor_data["Data"]["Pitch"] = "{0:.2f}".format(self.pitch)
        self.sensor_data["Data"]["Roll"] = "{0:.2f}".format(self.roll)
        self.sensor_data["Data"]["Yaw"] = "{0:.2f}".format(self.yaw)
        self.sensor_data["Data"]["G-Force"] = "{0:.1f}".format(self.gforce)
        self.sensor_data["Data"]["Max G-Force"] = "{0:.1f}".format(self.max_gforce)
        self.sensor_data["Data"]["Max Degree of Incline"] = "{0:.2f}".format(self.max_degree_of_incline)
        self.sensor_data["Data"]["Min Degree of Incline"] = "{0:.2f}".format(self.min_degree_of_incline)

        self.write_data(self.sensor_data)

    def disconnected(self):
        # Open connection via I2C
        self.connect_via_i2c()

        # Clear out real-time data
        self.sensor_data["Data"]["Pitch"] = None
        self.sensor_data["Data"]["Roll"] = None
        self.sensor_data["Data"]["Yaw"] = None
        self.sensor_data["Data"]["G-Force"] = None

        self.write_data(self.sensor_data)

        try:
            # Attempt connection to sensor
            self.bno = adafruit_bno055.BNO055_I2C(self.i2c)
        except Exception as e:
            self.logger.warning("%s: Failed trying to connected to sensor.", self.sensor)
            raise e

        # Connection acquired
        self.logger.debug("%s: Connection acquired.", self.sensor)
        self.update_state(State.INITIALIZE)

    def _set_values(self, linear_acceleration, euler_angle):
        # Setting G-Force
        if linear_acceleration[0] is not None and linear_acceleration[1] is not None and linear_acceleration[2] is not None:
            self.gforce = convert_linear_accl_to_gforce(linear_acceleration)

            # Set max g-force
            if not self.max_gforce or self.gforce > self.max_gforce:
                self.max_gforce = self.gforce

        if euler_angle[0] is not None and euler_angle[1] is not None and euler_angle[2] is not None:
            self.pitch = euler_angle[2]
            self.roll = euler_angle[1]
            self.yaw = euler_angle[0]

            # Set max and min degree of incline
            if not self.max_degree_of_incline or self.pitch > self.max_degree_of_incline:
                self.max_degree_of_incline = self.pitch
            if not self.min_degree_of_incline or self.pitch < self.min_degree_of_incline:
                self.min_degree_of_incline = self.pitch
