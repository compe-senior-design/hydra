import adafruit_bmp3xx
from SensorModules.I2CSensor import I2CSensor
from SensorModules.States import State
from Utility.sensor_utilities import convert_meters_to_feet


# BMP388 Sensor
class Altimeter(I2CSensor):

    def __init__(self, PRESSURE_SEA_LEVEL, FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG=False):
        super(Altimeter, self).__init__(FREQ_OF_DATA_READS, FREQ_OF_TRYING_TO_CONNECT, DEBUG)
        self.sensor = "BMP388"
        self.bmp = None
        self.current_altitude = None
        self.change_in_altitude = None
        self.starting_altitude = None
        self.max_altitude = None
        self.min_altitude = None
        self.pressure_sea_level = PRESSURE_SEA_LEVEL

        self.sensor_data = {"Sensor": self.sensor,
                            "State": State.DISCONNECTED,
                            "Data": {
                                "Altitude": self.current_altitude,
                                "Change in Altitude": self.change_in_altitude,
                                "Max Altitude": self.max_altitude,
                                "Min Altitude": self.min_altitude
                            }
                            }
        
        self.write_data(self.sensor_data)

    def read_from_sensor(self):

        try:
            altitude = self.bmp.altitude
        except Exception as e:
            self.logger.warning("%s: Failed trying read data from sensor.", self.sensor)
            raise e

        # Set the altitudes
        self._set_altitudes(altitude)

    def update_data(self):
        self.read_from_sensor()

        self.sensor_data["Data"]["Altitude"] = "{:.0f}".format(convert_meters_to_feet(self.current_altitude))
        self.sensor_data["Data"]["Change in Altitude"] = "{:.0f}".format(convert_meters_to_feet(self.change_in_altitude))
        self.sensor_data["Data"]["Max Altitude"] = "{:.0f}".format(convert_meters_to_feet(self.max_altitude))
        self.sensor_data["Data"]["Min Altitude"] = "{:.0f}".format(convert_meters_to_feet(self.min_altitude))

        self.write_data(self.sensor_data)

    def disconnected(self):
        # Open connection via I2C
        self.connect_via_i2c()

        # Clear out real-time data
        self.sensor_data["Data"]["Altitude"] = None
        self.write_data(self.sensor_data)

        try:
            # Attempt connection to sensor
            self.bmp = adafruit_bmp3xx.BMP3XX_I2C(self.i2c)
            self.bmp.sea_level_pressure = self.pressure_sea_level
        except Exception as e:
            self.logger.warning("%s: Failed trying to connected to sensor.", self.sensor)
            raise e

        # Connection acquired
        self.logger.debug("%s: Connection acquired.", self.sensor)
        self.update_state(State.INITIALIZE)

    def _set_altitudes(self, altitude):

        # Set starting altitude
        if self.current_altitude is None:
            self.starting_altitude = altitude

        self.change_in_altitude = altitude - self.starting_altitude

        # Set current altitude
        self.current_altitude = altitude

        # Set max and min altitudes
        if not self.max_altitude or self.current_altitude > self.max_altitude:
            self.max_altitude = self.current_altitude
        if not self.min_altitude or self.current_altitude < self.min_altitude:
            self.min_altitude = self.current_altitude
