import threading
import time
import sys

sys.path.append("/home/pi/Documents/senior_design/hydra")

from SensorModules.GPS import GPS
from SensorModules.Altimeter import Altimeter
from SensorModules.AmbientTemp import AmbientTemp
from SensorModules.Compass import Compass
from SensorModules.Gyroscope import Gyroscope

# Creating sensor objects
sensor_altimeter = Altimeter(500, 5000, True)
sensor_gps = GPS(500, 5000, True)
sensor_ambient = AmbientTemp(500, 5000, True)
sensor_compass = Compass(500, 5000, True)
sensor_gyro = Gyroscope(500, 5000, True)

# Spin up threads to run each sensor state machine
thread_altimeter = threading.Thread(target=sensor_altimeter.run)
thread_ambient_temp = threading.Thread(target=sensor_ambient.run)
thread_compass = threading.Thread(target=sensor_compass.run)
thread_gps = threading.Thread(target=sensor_gps.run)
thread_gyro = threading.Thread(target=sensor_gyro.run)

thread_altimeter.start()
thread_ambient_temp.start()
thread_compass.start()
thread_gps.start()
thread_gyro.start()

while True:
    # Read the data from the altimeter
    data_altimeter = sensor_altimeter.read_data()

    # Read the data from the ambient temperature sensor
    data_ambient = sensor_ambient.read_data()

    # Read the data from the compass
    data_compass = sensor_compass.read_data()

    # Read the data from the GPS
    data_gps = sensor_gps.read_data()

    # Read the data from the accelerometer/gyroscope
    data_gyro = sensor_gyro.read_data()

    print(str(data_altimeter["Sensor"]) + ": " + data_altimeter["State"].name)
    print(str(data_ambient["Sensor"]) + ": " + data_ambient["State"].name)
    print(str(data_compass["Sensor"]) + ": " + data_compass["State"].name)
    print(str(data_gps["Sensor"]) + ": " + data_gps["State"].name)
    print(str(data_gyro["Sensor"]) + ": " + data_gyro["State"].name)
    print()

    # Update every 3 seconds
    time.sleep(3)