# LSM303 DLHC
import board
import busio
from time import sleep
import adafruit_lsm303dlh_mag

i2c = busio.I2C(board.SCL, board.SDA)
mag = adafruit_lsm303dlh_mag.LSM303DLH_Mag(i2c)
print("Magnetometer (micro-Teslas)): X=%0.3f Y=%0.3f Z=%0.3f"%mag.magnetic)
counter = 0
max_X = 0
min_X = 0
max_Y = 0
min_Y = 0
max_Z = 0
min_Z = 0

while True:
    sleep(0.1)
    counter += 1
    X = mag.magnetic[0]
    Y = mag.magnetic[1]
    Z = mag.magnetic[2]

    if X >= max_X:
        max_X = X
    if X <= min_X:
        min_X = X
    if Y >= max_Y:
        max_Y = Y
    if Y <= min_Y:
        min_Y = Y
    if Z >= max_Z:
        max_Z = Z
    if Z <= min_Z:
        min_Z = Z

    if counter == 50:
        print("Max X: " + str(max_X) + "  Min X: " +  str(min_X))
        print("Max Y: " + str(max_Y) + "  Min Y: " +  str(min_Y))
        print("Max Z: " + str(max_Z) + "  Min Z: " +  str(min_Z))
        print()
        print()
        counter = 0