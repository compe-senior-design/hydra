from gpiozero import LED, Button
from signal import pause


def menu_pressed():
    print("Menu pressed")


def enter_pressed():
    print("Enter pressed")


def up_pressed():
    print("Up pressed")


def down_pressed():
    print("Down pressed")


if __name__ == "__main__":
    button_menu = Button(23)
    button_enter = Button(22)
    button_up = Button(27)
    button_down = Button(17)

    button_menu.when_released = menu_pressed
    button_enter.when_released = enter_pressed
    button_up.when_released = up_pressed
    button_down.when_released = down_pressed

    pause()
