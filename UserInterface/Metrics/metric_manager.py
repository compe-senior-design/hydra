# PyQt Packages
from PyQt5.QtCore import QObject

from UserInterface.layout_info import layout_file_name, equal_layout_sizes, prioritize_first_metric

from configparser import ConfigParser
import json


class MetricManager(QObject):
    def __init__(self, widget_list):
        super(MetricManager, self).__init__()

        self.widgets = widget_list
        self.layout_type = 0
        self.shown_count = len(widget_list)

        self.start_up_layout = 0

        self.default_layout = False
        self.default_priorities = []

        self._load_startup_layout()
        self._load_layout(self.start_up_layout)

        # Initial display of the widgets
        self._update_widgets()

    def _update_widgets(self):
        layout_dict = None
        if self.layout_type == 0:
            layout_dict = equal_layout_sizes[self.shown_count - 1]
        else:
            layout_dict = prioritize_first_metric[self.shown_count - 1]

        for widget in self.widgets:
            if widget.get_layout_priority() < self.shown_count:
                widget_dict = layout_dict[widget.get_layout_priority()]
                widget.set_position(widget_dict["x"], widget_dict["y"])
                widget.set_view(widget_dict["view_size"])
                widget.show()
            else:
                widget.hide()

    def _swap_widget_priorities(self, metric_selected: int, movement: int):
        self.default_layout = False

        for widget in self.widgets:
            if widget.get_layout_priority() == metric_selected:
                widget.set_layout_priority((metric_selected + movement) % self.shown_count)
            elif widget.get_layout_priority() == (metric_selected + movement) % self.shown_count:
                widget.set_layout_priority(metric_selected)

    def default_show_widget(self, hidden_widget):
        # Ensure widget is hidden and default layout is loaded
        if hidden_widget.get_layout_priority() < self.shown_count or not self.default_layout:
            return False

        current_priority = hidden_widget.get_layout_priority()

        # Get index of hidden widget
        index_of_hidden_widget = 0

        for index, widget in enumerate(self.widgets):
            if widget.get_layout_priority() == current_priority:
                index_of_hidden_widget = index
                break

        # Get original priority of this hidden widget and initialize new priority
        original_priority = self.default_priorities[index_of_hidden_widget]
        new_priority = 0

        # Go through widgets and find place of where to insert this hidden widget
        for index, widget in enumerate(self.widgets):
            # If widget is being shown check if this hidden widget should be placed after it or before it
            if widget.get_layout_priority() < self.shown_count:
                if self.default_priorities[index] < original_priority:
                    new_priority += 1
                else:
                    widget.set_layout_priority(widget.get_layout_priority() + 1)
            elif widget.get_layout_priority() < hidden_widget.get_layout_priority():
                widget.set_layout_priority(widget.get_layout_priority() + 1)

        # Set hidden widget to new priority
        hidden_widget.set_layout_priority(new_priority)

        # Increase shown count
        self.shown_count += 1

        # Update widgets
        self._update_widgets()

        # Return True to indicate the action happened
        return True

    def _show_widget(self, show_hidden_selected: int):
        if self.shown_count < len(self.widgets):
            self.default_layout = False

            hidden_widget_count = 0
            index_of_hidden_widget = 0
            index_swap = 0

            for index, widget in enumerate(self.widgets):
                if widget.get_layout_priority() >= self.shown_count:
                    hidden_widget_count += 1
                    if hidden_widget_count == show_hidden_selected + 1:
                        index_of_hidden_widget = index
                if widget.get_layout_priority() == self.shown_count:
                    index_swap = index

            if index_of_hidden_widget != index_swap:
                old_priority = self.widgets[index_of_hidden_widget].get_layout_priority()
                self.widgets[index_of_hidden_widget].set_layout_priority(
                    self.widgets[index_swap].get_layout_priority()
                )
                self.widgets[index_swap].set_layout_priority(old_priority)

            self.shown_count += 1

    def default_hide_widget(self, widget_to_hide):
        # Ensure widget is shown and default layout is loaded and this is not the last connected widget
        if widget_to_hide.get_layout_priority() >= self.shown_count or not self.default_layout or self.shown_count == 1:
            return False

        current_priority = widget_to_hide.get_layout_priority()

        # Go through widgets and increment priority of all widgets between current_priority and count of widgets - 1
        # (shifting them up in the display order)
        for widget in self.widgets:
            if (widget.get_layout_priority() > current_priority) and (widget.get_layout_priority() <= len(self.widgets) - 1):
                widget.set_layout_priority(widget.get_layout_priority() - 1)

        # Set widget to hide to end priority
        widget_to_hide.set_layout_priority(len(self.widgets) - 1)

        # Decrease shown count
        self.shown_count -= 1

        # Update widgets
        self._update_widgets()

        # Return True to indicate the action happened
        return True

    def _hide_widget(self, metric_selected: int):
        if self.shown_count > 1:
            self.default_layout = False

            for widget in self.widgets:
                if widget.get_layout_priority() == metric_selected:
                    widget.set_layout_priority(len(self.widgets) - 1)
                elif widget.get_layout_priority() > metric_selected:
                    widget.set_layout_priority(widget.get_layout_priority() - 1)

            self.shown_count -= 1

    def _load_layout(self, layout_number: int):
        self.default_layout = False

        file_parser = ConfigParser()
        file_parser.read(layout_file_name)

        layout_string = ""
        if layout_number == 1:
            layout_string = file_parser.get("Layouts", "Layout1")
        elif layout_number == 2:
            layout_string = file_parser.get("Layouts", "Layout2")
        elif layout_number == 3:
            layout_string = file_parser.get("Layouts", "Layout3")
        else:
            layout_string = file_parser.get("Layouts", "Default")
            self.default_layout = True

        self.default_priorities = []

        if layout_string == "":
            for index, widget in enumerate(self.widgets):
                widget.set_layout_priority(index)
                if self.default_layout:
                    self.default_priorities.append(index)

            self.shown_count = len(self.widgets)
            self.layout_type = 0
        else:
            layout_json = json.loads(layout_string)
            priorities = layout_json["priorities"]
            for index, widget in enumerate(self.widgets):
                widget.set_layout_priority(priorities[index])
                if self.default_layout:
                    self.default_priorities.append(priorities[index])

            self.shown_count = layout_json["shown_count"]
            self.layout_type = layout_json["layout_type"]

    def _save_layout(self, save_selected: int):
        priorities = []
        for widget in self.widgets:
            priorities.append(widget.get_layout_priority())

        layout = {"shown_count": self.shown_count, "layout_type": self.layout_type, "priorities": priorities}

        layout_slot_str = ""
        if save_selected == 0:
            layout_slot_str = "Layout1"
        elif save_selected == 1:
            layout_slot_str = "Layout2"
        else:
            layout_slot_str = "Layout3"

        file_parser = ConfigParser()
        file_parser.read(layout_file_name)
        file_parser.set("Layouts", layout_slot_str, json.dumps(layout))

        # Write to layout file
        with open(layout_file_name, 'w') as layout_file:
            file_parser.write(layout_file)

    def _load_startup_layout(self):
        file_parser = ConfigParser()
        file_parser.read(layout_file_name)

        self.start_up_layout = file_parser.getint("Layouts", "startup_layout")

    def _save_startup_layout(self):
        file_parser = ConfigParser()
        file_parser.read(layout_file_name)
        file_parser.set("Layouts", "startup_layout", str(self.start_up_layout))

        # Write to layout file
        with open(layout_file_name, 'w') as layout_file:
            file_parser.write(layout_file)
