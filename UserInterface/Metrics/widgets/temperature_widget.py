from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, add_widget_and_show


# This is the class that implements the custom widget.
class TemperatureWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(TemperatureWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()

        # Graphics views
        self.temperature_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/temperature.png")
        self.temperature_pixmap = QtGui.QPixmap()
        self.temperature_pixmap.convertFromImage(self.temperature_img)
        self.temperature = QtWidgets.QLabel()
        self.temperature.setPixmap(self.temperature_pixmap)

        self.temperature_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/temperature_small.png")
        self.temperature_small_pixmap = QtGui.QPixmap()
        self.temperature_small_pixmap.convertFromImage(self.temperature_small_img)
        self.temperature_small = QtWidgets.QLabel()
        self.temperature_small.setPixmap(self.temperature_small_pixmap)

        # Static labels
        self.label_static_max = QtWidgets.QLabel()
        self.label_static_min = QtWidgets.QLabel()
        self.label_static_temperature = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_temperature = QtWidgets.QLabel()
        self.label_dyn_max_temperature = QtWidgets.QLabel()
        self.label_dyn_min_temperature = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.temperature.setAlignment(QtCore.Qt.AlignCenter)
        self.temperature_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_temperature.setFont(Fonts.MAIN_METRIC.value)
        self.label_dyn_cur_temperature.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_max.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_max_temperature.setFont(Fonts.SUB_METRIC.value)
        self.label_static_min.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_min_temperature.setFont(Fonts.SUB_METRIC.value)
        self.label_static_temperature.setFont(Fonts.TITLE.value)
        self.label_static_temperature.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_max.setText("Max:")
        self.label_static_min.setText("Min:")
        self.label_static_temperature.setText("TEMPERATURE")

        self.label_dyn_cur_temperature.setText("---")
        self.label_dyn_max_temperature.setText("---")
        self.label_dyn_min_temperature.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.temperature)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current temperature
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_temperature)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Max: "
        add_widget_and_show(self.horizontalLayout, self.label_static_max)

        # Add a dynamic label for max temperature
        add_widget_and_show(self.horizontalLayout, self.label_dyn_max_temperature)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Min: "
        add_widget_and_show(self.horizontalLayout, self.label_static_min)

        # Add a dynamic label for min temperature
        add_widget_and_show(self.horizontalLayout, self.label_dyn_min_temperature)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "TEMPERATURE"
        add_widget_and_show(self.verticalLayout, self.label_static_temperature)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.temperature_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current temperature
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_temperature)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Max: "
        add_widget_and_show(self.horizontalLayout, self.label_static_max)

        # Add a dynamic label for max temperature
        add_widget_and_show(self.horizontalLayout, self.label_dyn_max_temperature)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Min: "
        add_widget_and_show(self.horizontalLayout, self.label_static_min)

        # Add a dynamic label for min temperature
        add_widget_and_show(self.horizontalLayout, self.label_dyn_min_temperature)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "TEMPERATURE"
        add_widget_and_show(self.verticalLayout, self.label_static_temperature)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a dynamic label for current temperature
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_temperature)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Max: "
        add_widget_and_show(self.horizontalLayout, self.label_static_max)

        # Add a dynamic label for max temperature
        add_widget_and_show(self.horizontalLayout, self.label_dyn_max_temperature)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Min: "
        add_widget_and_show(self.horizontalLayout, self.label_static_min)

        # Add a dynamic label for min temperature
        add_widget_and_show(self.horizontalLayout, self.label_dyn_min_temperature)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "TEMPERATURE"
        add_widget_and_show(self.verticalLayout, self.label_static_temperature)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a dynamic label for current temperature
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_temperature)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "TEMPERATURE"
        add_widget_and_show(self.verticalLayout, self.label_static_temperature)

    def update_data(self, data_dict):
        self.label_dyn_cur_temperature.setText(data_dict["Current Temperature"])
        self.label_dyn_max_temperature.setText(data_dict["High Temp"])
        self.label_dyn_min_temperature.setText(data_dict["Low Temp"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "Temperature"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = TemperatureWidget()
    demo.show()

    sys.exit(app.exec_())
