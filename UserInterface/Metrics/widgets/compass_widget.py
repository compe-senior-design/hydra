from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, add_widget_and_show


# This is the class that implements the custom widget.
class CompassWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(CompassWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()

        # Graphics views
        self.compass_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/compass.png")
        self.compass_pixmap = QtGui.QPixmap()
        self.compass_pixmap.convertFromImage(self.compass_img)
        self.compass = QtWidgets.QLabel()
        self.compass.setPixmap(self.compass_pixmap)

        self.compass_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/compass_small.png")
        self.compass_small_pixmap = QtGui.QPixmap()
        self.compass_small_pixmap.convertFromImage(self.compass_small_img)
        self.compass_small = QtWidgets.QLabel()
        self.compass_small.setPixmap(self.compass_small_pixmap)

        # Static labels
        self.label_static_compass = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_cardinal_dir = QtWidgets.QLabel()
        self.label_dyn_cur_deg_dir = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.compass.setAlignment(QtCore.Qt.AlignCenter)
        self.compass.setFixedWidth(380)
        self.compass_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_cardinal_dir.setFont(Fonts.MAIN_METRIC.value)
        self.label_dyn_cur_cardinal_dir.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_deg_dir.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_cur_deg_dir.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_compass.setFont(Fonts.TITLE.value)
        self.label_static_compass.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_compass.setText("COMPASS")

        self.label_dyn_cur_cardinal_dir.setText("---")
        self.label_dyn_cur_deg_dir.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a graphics view
        add_widget_and_show(self.horizontalLayout, self.compass)

        # Add a vertical layout (nested in horizontal layout)
        # Add an expanding spacer
        self.verticalLayout_2.addItem(Spacers.v_expanding())

        # Add a dynamic label for current cardinal direction
        add_widget_and_show(self.verticalLayout_2, self.label_dyn_cur_cardinal_dir)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a dynamic label for current direction in degrees
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_cur_deg_dir)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        # Add an expanding spacer
        self.verticalLayout_2.addItem(Spacers.v_expanding())

        # Add the vertical layout (now filled in) to the horizontal layout
        self.horizontalLayout.addLayout(self.verticalLayout_2)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a static label to display "COMPASS"
        add_widget_and_show(self.verticalLayout, self.label_static_compass)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.compass_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current cardinal direction
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_cardinal_dir)

        # Add a dynamic label for current direction in degrees
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_deg_dir)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "COMPASS"
        add_widget_and_show(self.verticalLayout, self.label_static_compass)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a dynamic label for current cardinal direction
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_cardinal_dir)

        # Add a dynamic label for current direction in degrees
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_deg_dir)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "COMPASS"
        add_widget_and_show(self.verticalLayout, self.label_static_compass)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a dynamic label for current cardinal direction
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_cardinal_dir)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "COMPASS"
        add_widget_and_show(self.verticalLayout, self.label_static_compass)

    def update_data(self, data_dict):
        # Update text
        self.label_dyn_cur_cardinal_dir.setText(data_dict["Cardinal Direction"])
        self.label_dyn_cur_deg_dir.setText(data_dict["Degree Direction"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "Compass"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = CompassWidget()
    demo.show()

    sys.exit(app.exec_())
