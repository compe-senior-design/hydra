from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, add_widget_and_show


# This is the class that implements the custom widget.
class TimeWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(TimeWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()

        # Graphics views
        self.time_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/time.png")
        self.time_pixmap = QtGui.QPixmap()
        self.time_pixmap.convertFromImage(self.time_img)
        self.time = QtWidgets.QLabel()
        self.time.setPixmap(self.time_pixmap)

        self.time_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/time_small.png")
        self.time_small_pixmap = QtGui.QPixmap()
        self.time_small_pixmap.convertFromImage(self.time_small_img)
        self.time_small = QtWidgets.QLabel()
        self.time_small.setPixmap(self.time_small_pixmap)

        # Static labels
        self.label_static_lap = QtWidgets.QLabel()
        self.label_static_time = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_time = QtWidgets.QLabel()
        self.label_dyn_lap_time = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.time.setAlignment(QtCore.Qt.AlignCenter)
        self.time.setFixedWidth(380)
        self.time_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_time.setFont(Fonts.MAIN_METRIC.value)
        self.label_dyn_cur_time.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_lap.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_lap_time.setFont(Fonts.SUB_METRIC.value)
        self.label_static_time.setFont(Fonts.TITLE.value)
        self.label_static_time.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_lap.setText("Lap:")
        self.label_static_time.setText("TIME")

        self.label_dyn_cur_time.setText("---")
        self.label_dyn_lap_time.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a horizontal layout (nested in the vertical layout)
        # Add a graphics view
        add_widget_and_show(self.horizontalLayout, self.time)

        # Add a vertical layout (nested in horizontal layout)
        # Add an expanding spacer
        self.verticalLayout_2.addItem(Spacers.v_expanding())

        # Add a dynamic label for current time
        add_widget_and_show(self.verticalLayout_2, self.label_dyn_cur_time)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Lap:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_lap)

        # Add a dynamic label to display the lap time
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_lap_time)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        # Add an expanding spacer
        self.verticalLayout_2.addItem(Spacers.v_expanding())

        # Add the vertical layout (now filled in) to the horizontal layout
        self.horizontalLayout.addLayout(self.verticalLayout_2)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a static label to display "TIME"
        add_widget_and_show(self.verticalLayout, self.label_static_time)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.time_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current time
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_time)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Lap:"
        add_widget_and_show(self.horizontalLayout, self.label_static_lap)

        # Add a dynamic label to display the lap time
        add_widget_and_show(self.horizontalLayout, self.label_dyn_lap_time)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "TIME"
        add_widget_and_show(self.verticalLayout, self.label_static_time)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a dynamic label for current time
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_time)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Lap:"
        add_widget_and_show(self.horizontalLayout, self.label_static_lap)

        # Add a dynamic label to display the lap time
        add_widget_and_show(self.horizontalLayout, self.label_dyn_lap_time)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "TIME"
        add_widget_and_show(self.verticalLayout, self.label_static_time)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a dynamic label for current time
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_time)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "TIME"
        add_widget_and_show(self.verticalLayout, self.label_static_time)

    def update_data(self, data_dict):
        self.label_dyn_cur_time.setText(data_dict["Time"])
        self.label_dyn_lap_time.setText(data_dict["Lap Time"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "Time"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = TimeWidget()
    demo.show()

    sys.exit(app.exec_())
