from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QApplication, QSpacerItem, QSizePolicy
from enum import Enum


# Enumeration for view sizes
# Dimensions given by tuple (width, height)
class ViewSize(Enum):
    FULL = (800, 480)
    HALF = (400, 480)
    FOURTH = (400, 240)
    EIGHTH = (200, 240)


# Enumeration for fonts used in GUI
class Fonts(Enum):
    MAIN_METRIC = QFont("Agency FB", 48)
    MAIN_METRIC_2 = QFont("Agency FB", 40)
    SUB_METRIC = QFont("Agency FB", 20)
    TITLE = QFont("Agency FB", 24, 75)


# Functions to get copies of spacers used in GUI
class Spacers:
    @staticmethod
    def v_expanding():
        return QSpacerItem(20, 20, QSizePolicy.Fixed, QSizePolicy.Expanding)

    @staticmethod
    def h_expanding():
        return QSpacerItem(20, 20, QSizePolicy.Expanding, QSizePolicy.Fixed)

    @staticmethod
    def v_fixed():
        return QSpacerItem(20, 20, QSizePolicy.Fixed, QSizePolicy.Fixed)

    @staticmethod
    def h_fixed():
        return QSpacerItem(20, 20, QSizePolicy.Fixed, QSizePolicy.Fixed)

    @staticmethod
    def v_expanding_min():
        return QSpacerItem(20, 20, QSizePolicy.Fixed, QSizePolicy.MinimumExpanding)

    @staticmethod
    def h_expanding_min():
        return QSpacerItem(20, 20, QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)


def add_widget_and_show(layout, widget):
    layout.addWidget(widget)
    widget.show()


# This is the class that implements the base custom widget.
# TODO: Eventually this constructor should require the view size to be specified
class StatWidget(QWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(StatWidget, self).__init__(parent)

        # This value dictates where in the layout this widget falls
        self.layout_priority = 0

    def set_layout_priority(self, value: int):
        self.layout_priority = value

    def get_layout_priority(self):
        return self.layout_priority

    def set_position(self, top_left_x: int, top_left_y: int):
        self.move(top_left_x, top_left_y)

    def set_view(self, view: ViewSize):
        # Update the view size variable
        self.view_size = view
        # Call the correct resize method
        if self.view_size == ViewSize.FULL:
            self.set_view_full()
        elif self.view_size == ViewSize.HALF:
            self.set_view_half()
        elif self.view_size == ViewSize.FOURTH:
            self.set_view_fourth()
        else:
            self.set_view_eighth()

    # Abstract functions (implementation handled by derived class)
    def set_view_full(self):
        pass

    def set_view_half(self):
        pass

    def set_view_fourth(self):
        pass

    def set_view_eighth(self):
        pass

    def update_data(self, data_dict):
        pass

    def _remove_all_layout_items(self, layout):
        while layout.count() > 0:
            # Remove the first element from the layout
            element = layout.takeAt(0)
            # If the element is a widget, set its parent to None
            if element.widget():
                element.widget().hide()
                element.widget().setParent(None)
            # If the element is a layout, remove all children elements recursively
            elif element.layout():
                self._remove_all_layout_items(element)
