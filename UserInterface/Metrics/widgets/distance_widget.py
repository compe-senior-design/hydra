from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, ViewSize, add_widget_and_show


# This is the class that implements the custom widget.
class DistanceWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(DistanceWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()

        # Graphics views
        self.distance_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/distance.png")
        self.distance_pixmap = QtGui.QPixmap()
        self.distance_pixmap.convertFromImage(self.distance_img)
        self.distance = QtWidgets.QLabel()
        self.distance.setPixmap(self.distance_pixmap)

        self.distance_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/distance_small.png")
        self.distance_small_pixmap = QtGui.QPixmap()
        self.distance_small_pixmap.convertFromImage(self.distance_small_img)
        self.distance_small = QtWidgets.QLabel()
        self.distance_small.setPixmap(self.distance_small_pixmap)

        # Static labels
        self.label_static_lat = QtWidgets.QLabel()
        self.label_static_long = QtWidgets.QLabel()
        self.label_static_distance = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_distance = QtWidgets.QLabel()
        self.label_dyn_lat = QtWidgets.QLabel()
        self.label_dyn_long = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.distance.setAlignment(QtCore.Qt.AlignCenter)
        self.distance.setFixedWidth(380)
        self.distance_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_distance.setFont(Fonts.MAIN_METRIC.value)
        self.label_dyn_cur_distance.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_lat.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_lat.setFont(Fonts.SUB_METRIC.value)
        self.label_static_long.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_long.setFont(Fonts.SUB_METRIC.value)
        self.label_static_distance.setFont(Fonts.TITLE.value)
        self.label_static_distance.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_lat.setText("Lat:")
        self.label_static_long.setText("Long:")
        self.label_static_distance.setText("DISTANCE")

        self.label_dyn_cur_distance.setText("---")
        self.label_dyn_lat.setText("---")
        self.label_dyn_long.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add a graphics view
        add_widget_and_show(self.horizontalLayout, self.distance)

        # Add a vertical layout (nested in horizontal layout)
        # Add an expanding spacer
        self.verticalLayout_2.addItem(Spacers.v_expanding())

        # Add a dynamic label for current distance
        add_widget_and_show(self.verticalLayout_2, self.label_dyn_cur_distance)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Lat:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_lat)

        # Add a dynamic label to display the latitude
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_lat)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_3.addItem(Spacers.h_expanding())

        # Add a static label to display "Long:"
        add_widget_and_show(self.horizontalLayout_3, self.label_static_long)

        # Add a dynamic label to display the longitude
        add_widget_and_show(self.horizontalLayout_3, self.label_dyn_long)

        # Add an expanding spacer
        self.horizontalLayout_3.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        # Add an expanding spacer
        self.verticalLayout_2.addItem(Spacers.v_expanding())

        # Add the vertical layout (now filled in) to the horizontal layout
        self.horizontalLayout.addLayout(self.verticalLayout_2)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a static label to display "DISTANCE"
        add_widget_and_show(self.verticalLayout, self.label_static_distance)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.distance_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current distance
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_distance)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Lat:"
        add_widget_and_show(self.horizontalLayout, self.label_static_lat)

        # Add a dynamic label to display the latitude
        add_widget_and_show(self.horizontalLayout, self.label_dyn_lat)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Long:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_long)

        # Add a dynamic label to display the longitude
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_long)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "DISTANCE"
        add_widget_and_show(self.verticalLayout, self.label_static_distance)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a dynamic label for current distance
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_distance)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "DISTANCE"
        add_widget_and_show(self.verticalLayout, self.label_static_distance)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a dynamic label for current distance
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_distance)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "DISTANCE"
        add_widget_and_show(self.verticalLayout, self.label_static_distance)

    def update_data(self, data_dict):
        self.label_dyn_cur_distance.setText(data_dict["Distance Traveled"])
        self.label_dyn_lat.setText(data_dict["Latitude"])
        self.label_dyn_long.setText(data_dict["Longitude"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "Distance"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = DistanceWidget()
    demo.set_view(ViewSize.HALF)
    demo.show()

    sys.exit(app.exec_())
