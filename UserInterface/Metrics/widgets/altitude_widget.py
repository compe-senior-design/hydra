from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, add_widget_and_show

import threading


# This is the class that implements the custom widget.
class AltitudeWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(AltitudeWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()

        # Graphics views
        self.altitude_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/altitude.png")
        self.altitude_pixmap = QtGui.QPixmap()
        self.altitude_pixmap.convertFromImage(self.altitude_img)
        self.altitude = QtWidgets.QLabel()
        self.altitude.setPixmap(self.altitude_pixmap)

        self.altitude_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/altitude_small.png")
        self.altitude_small_pixmap = QtGui.QPixmap()
        self.altitude_small_pixmap.convertFromImage(self.altitude_small_img)
        self.altitude_small = QtWidgets.QLabel()
        self.altitude_small.setPixmap(self.altitude_small_pixmap)

        # Static labels
        self.label_static_change = QtWidgets.QLabel()
        self.label_static_max = QtWidgets.QLabel()
        self.label_static_min = QtWidgets.QLabel()
        self.label_static_altitude = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_altitude = QtWidgets.QLabel()
        self.label_dyn_change_altitude = QtWidgets.QLabel()
        self.label_dyn_max_altitude = QtWidgets.QLabel()
        self.label_dyn_min_altitude = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.altitude.setAlignment(QtCore.Qt.AlignCenter)
        self.altitude_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_altitude.setFont(Fonts.MAIN_METRIC.value)
        self.label_dyn_cur_altitude.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_change.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_change_altitude.setFont(Fonts.SUB_METRIC.value)
        self.label_static_max.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_max_altitude.setFont(Fonts.SUB_METRIC.value)
        self.label_static_min.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_min_altitude.setFont(Fonts.SUB_METRIC.value)
        self.label_static_altitude.setFont(Fonts.TITLE.value)
        self.label_static_altitude.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_change.setText("Change:")
        self.label_static_max.setText("Max:")
        self.label_static_min.setText("Min:")
        self.label_static_altitude.setText("ALTITUDE")

        self.label_dyn_cur_altitude.setText("---")
        self.label_dyn_change_altitude.setText("---")
        self.label_dyn_max_altitude.setText("---")
        self.label_dyn_min_altitude.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.altitude)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current altitude
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_altitude)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Change: "
        add_widget_and_show(self.horizontalLayout, self.label_static_change)

        # Add a dynamic label for change in altitude
        add_widget_and_show(self.horizontalLayout, self.label_dyn_change_altitude)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Max: "
        add_widget_and_show(self.horizontalLayout_2, self.label_static_max)

        # Add a dynamic label for max altitude
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_max_altitude)

        # Add a fixed spacer
        self.horizontalLayout_2.addItem(Spacers.h_fixed())

        # Add a static label to display "Min: "
        add_widget_and_show(self.horizontalLayout_2, self.label_static_min)

        # Add a dynamic label for min altitude
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_min_altitude)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "ALTITUDE"
        add_widget_and_show(self.verticalLayout, self.label_static_altitude)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.altitude_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current altitude
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_altitude)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Change: "
        add_widget_and_show(self.horizontalLayout, self.label_static_change)

        # Add a dynamic label for change in altitude
        add_widget_and_show(self.horizontalLayout, self.label_dyn_change_altitude)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Max: "
        add_widget_and_show(self.horizontalLayout_2, self.label_static_max)

        # Add a dynamic label for max altitude
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_max_altitude)

        # Add a fixed spacer
        self.horizontalLayout_2.addItem(Spacers.h_fixed())

        # Add a static label to display "Min: "
        add_widget_and_show(self.horizontalLayout_2, self.label_static_min)

        # Add a dynamic label for min altitude
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_min_altitude)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "ALTITUDE"
        add_widget_and_show(self.verticalLayout, self.label_static_altitude)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a dynamic label for current altitude
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_altitude)

        # Add a horizontal layout (nested in the vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Change: "
        add_widget_and_show(self.horizontalLayout, self.label_static_change)

        # Add a dynamic label to display the current change in altitude
        add_widget_and_show(self.horizontalLayout, self.label_dyn_change_altitude)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "ALTITUDE"
        add_widget_and_show(self.verticalLayout, self.label_static_altitude)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a dynamic label for current altitude
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_altitude)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "ALTITUDE"
        add_widget_and_show(self.verticalLayout, self.label_static_altitude)

    def update_data(self, data_dict):
        # Update UI values
        self.label_dyn_cur_altitude.setText(data_dict["Altitude"])
        self.label_dyn_change_altitude.setText(data_dict["Change Altitude"])
        self.label_dyn_max_altitude.setText(data_dict["Max Altitude"])
        self.label_dyn_min_altitude.setText(data_dict["Min Altitude"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "Altitude"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = AltitudeWidget()
    demo.show()

    sys.exit(app.exec_())
