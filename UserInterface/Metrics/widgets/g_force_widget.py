from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, add_widget_and_show


# This is the class that implements the custom widget.
class GForceWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(GForceWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()

        # Graphics views
        self.g_force_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/g-force.png")
        self.g_force_pixmap = QtGui.QPixmap()
        self.g_force_pixmap.convertFromImage(self.g_force_img)
        self.g_force = QtWidgets.QLabel()
        self.g_force.setPixmap(self.g_force_pixmap)

        self.g_force_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/g-force_small.png")
        self.g_force_small_pixmap = QtGui.QPixmap()
        self.g_force_small_pixmap.convertFromImage(self.g_force_small_img)
        self.g_force_small = QtWidgets.QLabel()
        self.g_force_small.setPixmap(self.g_force_small_pixmap)

        # Static labels
        self.label_static_max = QtWidgets.QLabel()
        self.label_static_g_force = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_g_force = QtWidgets.QLabel()
        self.label_dyn_max_g_force = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.g_force.setAlignment(QtCore.Qt.AlignCenter)
        self.g_force_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_dyn_cur_g_force.setFont(Fonts.MAIN_METRIC.value)
        self.label_dyn_cur_g_force.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_max.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_max_g_force.setFont(Fonts.SUB_METRIC.value)
        self.label_static_g_force.setFont(Fonts.TITLE.value)
        self.label_static_g_force.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_max.setText("Max:")
        self.label_static_g_force.setText("G-FORCE")

        self.label_dyn_cur_g_force.setText("---")
        self.label_dyn_max_g_force.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.g_force)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current G-Force
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_g_force)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Max:"
        add_widget_and_show(self.horizontalLayout, self.label_static_max)

        # Add a dynamic label to display the max G-Force
        add_widget_and_show(self.horizontalLayout, self.label_dyn_max_g_force)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "G-FORCE"
        add_widget_and_show(self.verticalLayout, self.label_static_g_force)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.g_force_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a dynamic label for current G-Force
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_g_force)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Max:"
        add_widget_and_show(self.horizontalLayout, self.label_static_max)

        # Add a dynamic label to display the max G-Force
        add_widget_and_show(self.horizontalLayout, self.label_dyn_max_g_force)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "G-FORCE"
        add_widget_and_show(self.verticalLayout, self.label_static_g_force)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a dynamic label for current G-Force
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_g_force)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Max:"
        add_widget_and_show(self.horizontalLayout, self.label_static_max)

        # Add a dynamic label to display the max G-Force
        add_widget_and_show(self.horizontalLayout, self.label_dyn_max_g_force)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "G-FORCE"
        add_widget_and_show(self.verticalLayout, self.label_static_g_force)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a dynamic label for current G-Force
        add_widget_and_show(self.verticalLayout, self.label_dyn_cur_g_force)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "G-FORCE"
        add_widget_and_show(self.verticalLayout, self.label_static_g_force)

    def update_data(self, data_dict):
        self.label_dyn_cur_g_force.setText(data_dict["G-Force"])
        self.label_dyn_max_g_force.setText(data_dict["Max G-Force"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "G-Force"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = GForceWidget()
    demo.show()

    sys.exit(app.exec_())
