from PyQt5 import QtCore, QtWidgets, QtGui
from UserInterface.Metrics.widgets.base_stat_widget import StatWidget, Spacers, Fonts, add_widget_and_show


# This is the class that implements the custom widget.
class OrientationWidget(StatWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(OrientationWidget, self).__init__(parent)

        # --- CREATE UI ELEMENTS --- #
        # Layouts
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()

        # Graphics views
        self.orientation_small_img = QtGui.QImage("UserInterface/Metrics/widgets/icons/orientation_small.png")
        self.orientation_small_pixmap = QtGui.QPixmap()
        self.orientation_small_pixmap.convertFromImage(self.orientation_small_img)
        self.orientation_small = QtWidgets.QLabel()
        self.orientation_small.setPixmap(self.orientation_small_pixmap)

        # Static labels
        self.label_static_yaw = QtWidgets.QLabel()
        self.label_static_pitch = QtWidgets.QLabel()
        self.label_static_roll = QtWidgets.QLabel()
        self.label_static_max_incline = QtWidgets.QLabel()
        self.label_static_min_incline = QtWidgets.QLabel()
        self.label_static_orientation = QtWidgets.QLabel()

        # Dynamic labels
        self.label_dyn_cur_yaw = QtWidgets.QLabel()
        self.label_dyn_cur_pitch = QtWidgets.QLabel()
        self.label_dyn_cur_roll = QtWidgets.QLabel()
        self.label_dyn_max_incline = QtWidgets.QLabel()
        self.label_dyn_min_incline = QtWidgets.QLabel()

        # --- FORMATTING --- #
        # Set the margins of the base vertical layout
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        # Set fonts and alignment of labels
        self.orientation_small.setAlignment(QtCore.Qt.AlignCenter)
        self.label_static_yaw.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_static_pitch.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_static_roll.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_static_max_incline.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_max_incline.setFont(Fonts.SUB_METRIC.value)
        self.label_static_min_incline.setFont(Fonts.SUB_METRIC.value)
        self.label_dyn_min_incline.setFont(Fonts.SUB_METRIC.value)
        self.label_static_orientation.setFont(Fonts.TITLE.value)
        self.label_static_orientation.setAlignment(QtCore.Qt.AlignCenter)

        # --- POPULATE DEFAULTS --- #
        self.label_static_yaw.setText("Yaw:")
        self.label_static_pitch.setText("Pitch:")
        self.label_static_roll.setText("Roll:")
        self.label_static_max_incline.setText("Max Incline:")
        self.label_static_min_incline.setText("Min Incline:")
        self.label_static_orientation.setText("ORIENTATION")

        self.label_dyn_cur_yaw.setText("---")
        self.label_dyn_cur_pitch.setText("---")
        self.label_dyn_cur_roll.setText("---")
        self.label_dyn_max_incline.setText("---")
        self.label_dyn_min_incline.setText("---")

    def set_view_full(self):
        # Set the size of the view to be full screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.orientation_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Yaw:"
        self.label_static_yaw.setFont(Fonts.MAIN_METRIC_2.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_yaw)

        # Add a dynamic label for current yaw
        self.label_dyn_cur_yaw.setFont(Fonts.MAIN_METRIC_2.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_yaw)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Pitch:"
        self.label_static_pitch.setFont(Fonts.MAIN_METRIC_2.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_pitch)

        # Add a dynamic label for current pitch
        self.label_dyn_cur_pitch.setFont(Fonts.MAIN_METRIC_2.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_pitch)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Roll:"
        self.label_static_roll.setFont(Fonts.MAIN_METRIC_2.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_roll)

        # Add a dynamic label for current roll
        self.label_dyn_cur_roll.setFont(Fonts.MAIN_METRIC_2.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_roll)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Max Incline:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_max_incline)

        # Add a dynamic label for maximum incline
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_max_incline)

        # Add a fixed spacer
        self.horizontalLayout_2.addItem(Spacers.h_fixed())

        # Add a static label to display "Min Incline:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_min_incline)

        # Add a dynamic label for minimum incline
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_min_incline)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "ORIENTATION"
        add_widget_and_show(self.verticalLayout, self.label_static_orientation)

    def set_view_half(self):
        # Set the size of the view to be 1/2 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a graphics view
        add_widget_and_show(self.verticalLayout, self.orientation_small)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add a static label to display "Yaw:"
        self.label_static_yaw.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_yaw)

        # Add a dynamic label for current yaw
        self.label_dyn_cur_yaw.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_yaw)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Pitch:"
        self.label_static_pitch.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_pitch)

        # Add a dynamic label for current pitch
        self.label_dyn_cur_pitch.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_pitch)

        # Add a fixed spacer
        self.horizontalLayout.addItem(Spacers.h_fixed())

        # Add a static label to display "Roll:"
        self.label_static_roll.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_roll)

        # Add a dynamic label for current roll
        self.label_dyn_cur_roll.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_roll)

        # Add an expanding spacer
        self.horizontalLayout.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add a static label to display "Max Incline:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_max_incline)

        # Add a dynamic label for maximum incline
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_max_incline)

        # Add a fixed spacer
        self.horizontalLayout_2.addItem(Spacers.h_fixed())

        # Add a static label to display "Min Incline:"
        add_widget_and_show(self.horizontalLayout_2, self.label_static_min_incline)

        # Add a dynamic label for minimum incline
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_min_incline)

        # Add an expanding spacer
        self.horizontalLayout_2.addItem(Spacers.h_expanding())

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a minimum expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding_min())

        # Add a static label to display "ORIENTATION"
        add_widget_and_show(self.verticalLayout, self.label_static_orientation)

    def set_view_fourth(self):
        # Set the size of the view to be 1/4 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add a static label to display "Yaw:"
        self.label_static_yaw.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_yaw)

        # Add a dynamic label for current yaw
        self.label_dyn_cur_yaw.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_yaw)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add a static label to display "Pitch:"
        self.label_static_pitch.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_2, self.label_static_pitch)

        # Add a dynamic label for current pitch
        self.label_dyn_cur_pitch.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_cur_pitch)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a horizontal layout (nested in vertical layout)
        # Add a static label to display "Roll:"
        self.label_static_roll.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_3, self.label_static_roll)

        # Add a dynamic label for current roll
        self.label_dyn_cur_roll.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_3, self.label_dyn_cur_roll)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "ORIENTATION"
        add_widget_and_show(self.verticalLayout, self.label_static_orientation)

    def set_view_eighth(self):
        # Set the size of the view to be 1/8 of the screen
        width, height = self.view_size.value
        self.resize(width, height)

        # Remove all previously added layout items
        self._remove_all_layout_items(self.verticalLayout)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a horizontal layout (nested in vertical layout)
        # Add a static label to display "Yaw:"
        self.label_static_yaw.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_static_yaw)

        # Add a dynamic label for current yaw
        self.label_dyn_cur_yaw.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout, self.label_dyn_cur_yaw)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Add a horizontal layout (nested in vertical layout)
        # Add a static label to display "Pitch:"
        self.label_static_pitch.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_2, self.label_static_pitch)

        # Add a dynamic label for current pitch
        self.label_dyn_cur_pitch.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_2, self.label_dyn_cur_pitch)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Add a horizontal layout (nested in vertical layout)
        # Add a static label to display "Roll:"
        self.label_static_roll.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_3, self.label_static_roll)

        # Add a dynamic label for current roll
        self.label_dyn_cur_roll.setFont(Fonts.SUB_METRIC.value)
        add_widget_and_show(self.horizontalLayout_3, self.label_dyn_cur_roll)

        # Add the horizontal layout (now filled in) to the vertical layout
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        # Add an expanding spacer
        self.verticalLayout.addItem(Spacers.v_expanding())

        # Add a static label to display "ORIENTATION"
        add_widget_and_show(self.verticalLayout, self.label_static_orientation)

    def update_data(self, data_dict):
        self.label_dyn_cur_yaw.setText(data_dict["Yaw"])
        self.label_dyn_cur_pitch.setText(data_dict["Pitch"])
        self.label_dyn_cur_roll.setText(data_dict["Roll"])
        self.label_dyn_max_incline.setText(data_dict["Max Degree of Incline"])
        self.label_dyn_min_incline.setText(data_dict["Min Degree of Incline"])

    # Unique Identifier
    @property
    def get_widget_name(self):
        return "Orientation"


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QtWidgets.QApplication(sys.argv)

    demo = OrientationWidget()
    demo.show()

    sys.exit(app.exec_())
