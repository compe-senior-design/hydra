from enum import Enum
from UserInterface.Metrics.widgets.base_stat_widget import ViewSize


class StoredLayouts(Enum):
    Default = 0
    Layout1 = 1
    Layout2 = 2
    Layout3 = 3


layout_file_name = "UserInterface/Metrics/layouts.ini"

equal_layout_sizes = [
    [  # 1 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FULL}
    ],
    [  # 2 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.HALF},
        {"x": 400, "y":   0, "view_size": ViewSize.HALF}
    ],
    [  # 3 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.HALF},
        {"x": 400, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y": 240, "view_size": ViewSize.FOURTH}
    ],
    [  # 4 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y":   0, "view_size": ViewSize.FOURTH},
        {"x":   0, "y": 240, "view_size": ViewSize.FOURTH},
        {"x": 400, "y": 240, "view_size": ViewSize.FOURTH}
    ],
    [  # 5 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y":   0, "view_size": ViewSize.FOURTH},
        {"x":   0, "y": 240, "view_size": ViewSize.FOURTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 6 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FOURTH},
        {"x":   0, "y": 240, "view_size": ViewSize.FOURTH},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 7 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x":   0, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 200, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 8 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 200, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x":   0, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 200, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ]
]

prioritize_first_metric = [
    [  # 1 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FULL}
    ],
    [  # 2 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.HALF},
        {"x": 400, "y":   0, "view_size": ViewSize.HALF}
    ],
    [  # 3 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.HALF},
        {"x": 400, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y": 240, "view_size": ViewSize.FOURTH}
    ],
    [  # 4 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.HALF},
        {"x": 400, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 5 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.HALF},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 6 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FOURTH},
        {"x":   0, "y": 240, "view_size": ViewSize.FOURTH},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 7 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.FOURTH},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x":   0, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 200, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ],
    [  # 8 Module UI
        {"x":   0, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 200, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y":   0, "view_size": ViewSize.EIGHTH},
        {"x":   0, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 200, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 400, "y": 240, "view_size": ViewSize.EIGHTH},
        {"x": 600, "y": 240, "view_size": ViewSize.EIGHTH}
    ]
]