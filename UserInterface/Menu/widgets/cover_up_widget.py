from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QPolygon, QPainter, QBrush
from PyQt5.QtCore import QPoint, Qt


# This is the class that implements the cover up widget.
class CoverUpWidget(QWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(CoverUpWidget, self).__init__(parent)

        self.arrow = 0

    def set_arrow_type(self, arrow_type: int):
        self.arrow = arrow_type

    def paintEvent(self, event):
        if self.arrow == 0:
            pass
        else:
            offset = 25
            geometry = self.geometry()
            points = None
            center_x = int(geometry.width() / 2)
            center_y = int(geometry.height() / 2)

            if self.arrow == 1:
                points = [
                    # Bottom Left
                    QPoint(center_x - offset, center_y + offset),
                    # Bottom Right
                    QPoint(center_x + offset, center_y + offset),
                    # Top Middle
                    QPoint(center_x, center_y - offset)
                ]
            else:
                points = [
                    # Top Left
                    QPoint(center_x - offset, center_y - offset),
                    # Top Right
                    QPoint(center_x + offset, center_y - offset),
                    # Bottom Middle
                    QPoint(center_x, center_y + offset)
                ]

            painter = QPainter()
            painter.begin(self)
            painter.setBrush(QBrush(Qt.black, Qt.SolidPattern))

            poly = QPolygon(points)
            painter.drawPolygon(poly)


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    demo = CoverUpWidget()
    demo.resize(800, 480)
    demo.show()
    sys.exit(app.exec_())
