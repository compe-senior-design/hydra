from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget


# This is the class that implements the debug buttons widget.
class DebugButtons(QWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(DebugButtons, self).__init__(parent)

        self.resize(50, 100)
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton_menu = QtWidgets.QPushButton(self)
        self.pushButton_menu.setObjectName("pushButton_menu")
        self.verticalLayout.addWidget(self.pushButton_menu)
        self.pushButton_menu.setText("Menu")
        self.pushButton_up = QtWidgets.QPushButton(self)
        self.pushButton_up.setObjectName("pushButton_up")
        self.verticalLayout.addWidget(self.pushButton_up)
        self.pushButton_up.setText("Up")
        self.pushButton_enter = QtWidgets.QPushButton(self)
        self.pushButton_enter.setObjectName("pushButton_enter")
        self.verticalLayout.addWidget(self.pushButton_enter)
        self.pushButton_enter.setText("Enter")
        self.pushButton_down = QtWidgets.QPushButton(self)
        self.pushButton_down.setObjectName("pushButton_down")
        self.verticalLayout.addWidget(self.pushButton_down)
        self.pushButton_down.setText("Down")
        self.pushButton_close = QtWidgets.QPushButton(self)
        self.pushButton_close.setObjectName("pushButton_close")
        self.verticalLayout.addWidget(self.pushButton_close)
        self.pushButton_close.setText("Close")


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    demo = DebugButtons()
    demo.show()

    sys.exit(app.exec_())
