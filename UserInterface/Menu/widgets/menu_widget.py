from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QWidget, QLabel


# This is the class that implements the base custom widget.
class BaseMenuWidget(QWidget):

    # Initialise the instance.
    def __init__(self, parent=None):
        super(BaseMenuWidget, self).__init__(parent)

        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)

        self.label_menu_title = QtWidgets.QLabel(self)
        self.label_menu_title.setFont(QtGui.QFont("Agency FB", 40))
        self.label_menu_title.setText("")
        self.verticalLayout.addWidget(self.label_menu_title)

        self.menu_labels = []

        for i in range(9):
            label = QtWidgets.QLabel(self)
            label.setFont(QtGui.QFont("Agency FB", 20))
            label.setText("")
            self.verticalLayout.addWidget(label)
            self.menu_labels.append(label)

    def set_label_menu_title(self, text: str):
        self.label_menu_title.setText(text)

    def set_label_menu_option(self, index: int, text: str):
        index = index % len(self.menu_labels)

        self.menu_labels[index].setText(text)

    def set_position(self, top_left_x: int, top_left_y: int):
        self.move(top_left_x, top_left_y)

    def set_size(self, width: int, height: int):
        self.resize(width, height)


# Display the custom widget if the script is being run directly from the
# command line.
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    demo = BaseMenuWidget()
    demo.resize(800, 480)
    demo.set_label_menu_title("Menu")
    demo.set_label_menu_option(0, "Post Ride Stats")
    demo.set_label_menu_option(1, "Hide Metric")
    demo.set_label_menu_option(2, "Show Metric")
    demo.set_label_menu_option(3, "Visual Preference")
    demo.set_label_menu_option(4, "Stored layout")
    demo.set_label_menu_option(5, "Save layout")
    demo.set_label_menu_option(6, "Load Layout")
    demo.set_label_menu_option(7, "Move Metric")
    demo.set_label_menu_option(8, "Credits")
    demo.show()

    sys.exit(app.exec_())
