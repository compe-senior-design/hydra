from UserInterface.Menu.widgets.menu_widget import BaseMenuWidget
from UserInterface.Menu.widgets.cover_up_widget import CoverUpWidget
from UserInterface.Metrics.metric_manager import MetricManager
from UserInterface.layout_info import StoredLayouts, equal_layout_sizes, prioritize_first_metric
from UserInterface.Metrics.widgets.base_stat_widget import ViewSize

# PyQt Packages
from PyQt5.QtGui import QColor
from PyQt5.QtCore import pyqtSignal

from enum import Enum


class MenuOptions(Enum):
    RideStats = 0
    HideMetric = 1
    ShowMetric = 2
    MoveMetric = 3
    LoadLayout = 4
    SaveLayout = 5
    StartupLayout = 6
    LayoutPreference = 7
    Credits = 8


class MenuStates(Enum):
    NoMenu = 0
    InMenu = 1
    RideStats = 2
    Hiding = 3
    Showing = 4
    Moving = 5
    MovingSelected = 6
    LoadingLayout = 7
    SavingLayout = 8
    InCredits = 9


class MenuManager(MetricManager):
    # -------- Signals -------- #
    request_update_stats = pyqtSignal()

    def __init__(self, widget_list):
        super(MenuManager, self).__init__(widget_list)

        self.menu_state = MenuStates.NoMenu

        self.ride_statistics = {}

        # Set up front end of menu
        self.menu = BaseMenuWidget(widget_list[0].parent())
        self.menu.set_position(0, 0)
        self.menu.set_size(800, 480)

        self.menu.setAutoFillBackground(True)
        p = self.menu.palette()
        p.setColor(self.menu.backgroundRole(), QColor(110, 110, 110, 255))
        self.menu.setPalette(p)

        self.menu.hide()

        self.cover_up_widgets = []
        for i in range(len(self.widgets)):
            cover_up_widget = CoverUpWidget(self.widgets[0].parent())

            cover_up_widget.setAutoFillBackground(True)
            p = cover_up_widget.palette()
            p.setColor(cover_up_widget.backgroundRole(), QColor(80, 80, 80, 127))
            cover_up_widget.setPalette(p)

            cover_up_widget.hide()
            self.cover_up_widgets.append(cover_up_widget)

        self.metric_selected = 0
        self.show_hidden_selected = 0
        self.menu_option_selected = 0
        self.menu_save_selected = 0
        self.menu_load_selected = 0
        self.ride_stat_selected = 0

    def _update_menu(self):
        if self.menu_state == MenuStates.NoMenu:
            for cover_up_widget in self.cover_up_widgets:
                cover_up_widget.hide()

            self.menu.hide()

        elif self.menu_state == MenuStates.InMenu:
            for cover_up_widget in self.cover_up_widgets:
                cover_up_widget.hide()

            self.menu.set_label_menu_title("Menu")

            startup_layout_str = ""
            if self.start_up_layout == 0:
                startup_layout_str = "Startup Layout = Default"
            elif self.start_up_layout == 1:
                startup_layout_str = "Startup Layout = Layout1"
            elif self.start_up_layout == 2:
                startup_layout_str = "Startup Layout = Layout2"
            else:
                startup_layout_str = "Startup Layout = Layout3"

            layout_preference_str = ""
            if self.layout_type == 0:
                layout_preference_str = "Preference = Equal Sized Metrics"
            else:
                layout_preference_str = "Preference = Optimize First Metric"

            menu_options = ["Ride Statistics", "Hide Metric", "Show Metric", "Move Metric", "Load Layout",
                            "Save Layout", startup_layout_str, layout_preference_str, "Credits"]

            for index, option in enumerate(menu_options):
                if index == self.menu_option_selected:
                    self.menu.set_label_menu_option(index, "<b>-> " + option + "</b>")
                else:
                    self.menu.set_label_menu_option(index, option)

            self.menu.show()

        elif self.menu_state == MenuStates.RideStats:
            self.menu.set_label_menu_title("Ride Statistics")

            label_count = 0

            for index, (key, value) in enumerate(self.ride_statistics.items()):
                if (index - self.ride_stat_selected) % len(self.ride_statistics) < len(MenuOptions):
                    self.menu.set_label_menu_option((index - self.ride_stat_selected) % len(self.ride_statistics), key + " : " + value)
                    label_count += 1

            # Fill menu with blank options if there are not enough ride stats
            for i in range(label_count, len(MenuOptions)):
                self.menu.set_label_menu_option(i, "")

            self.menu.show()

        elif self.menu_state == MenuStates.Hiding \
                or self.menu_state == MenuStates.Moving \
                or self.menu_state == MenuStates.MovingSelected:
            self.menu.hide()

            layout_dict = None
            if self.layout_type == 0:
                layout_dict = equal_layout_sizes[self.shown_count - 1]
            else:
                layout_dict = prioritize_first_metric[self.shown_count - 1]

            for index, cover_up_widget in enumerate(self.cover_up_widgets):
                if index < self.shown_count:
                    cover_up_widget.set_arrow_type(0)
                    if index == self.metric_selected:
                        cover_up_widget.hide()
                    else:
                        widget_dict = layout_dict[index]
                        cover_up_widget.move(widget_dict["x"], widget_dict["y"])
                        width, height = widget_dict["view_size"].value
                        cover_up_widget.resize(width, height)
                        cover_up_widget.repaint()
                        cover_up_widget.show()

                        if self.menu_state == MenuStates.MovingSelected and self.shown_count > 1:
                            if index == (self.metric_selected - 1) % self.shown_count:
                                cover_up_widget.set_arrow_type(1)
                                cover_up_widget.repaint()
                            elif index == (self.metric_selected + 1) % self.shown_count:
                                cover_up_widget.set_arrow_type(2)
                                cover_up_widget.repaint()
                else:
                    cover_up_widget.set_arrow_type(0)
                    cover_up_widget.hide()

        elif self.menu_state == MenuStates.Showing:
            self.menu.set_label_menu_title("Show Metric")

            hidden_widgets = []
            for widget in self.widgets:
                if widget.get_layout_priority() >= self.shown_count:
                    hidden_widgets.append(widget.get_widget_name)

            # Have to use modulus logic in case there are more metrics hidden than ui labels available
            for i in range(len(MenuOptions)):
                if i < len(hidden_widgets):
                    if i == self.show_hidden_selected:
                        self.menu.set_label_menu_option(i, "<b>-> " + hidden_widgets[i] + "</b>")
                    else:
                        self.menu.set_label_menu_option(i, hidden_widgets[i])
                else:
                    self.menu.set_label_menu_option(i, "")

            self.menu.show()

        elif self.menu_state == MenuStates.LoadingLayout:
            self.menu.set_label_menu_title("Load Layout")
            loadable_layouts = ["Default", "Layout1", "Layout2", "Layout3", "", "", "", "", ""]

            for index, layout in enumerate(loadable_layouts):
                if index == self.menu_load_selected:
                    self.menu.set_label_menu_option(index, "<b>-> " + layout + "</b>")
                else:
                    self.menu.set_label_menu_option(index, layout)

            self.menu.show()

        elif self.menu_state == MenuStates.SavingLayout:
            self.menu.set_label_menu_title("Save Layout")
            savable_layouts = ["Layout1", "Layout2", "Layout3", "", "", "", "", "", ""]

            for index, layout in enumerate(savable_layouts):
                if index == self.menu_save_selected:
                    self.menu.set_label_menu_option(index, "<b>-> " + layout + "</b>")
                else:
                    self.menu.set_label_menu_option(index, layout)

            self.menu.show()

        elif self.menu_state == MenuStates.InCredits:
            self.menu.set_label_menu_title("Credits")
            app_credits = ["Icon made by Freepik from www.flaticon.com",
                           "Icon made by DinosoftLabs from www.flaticon.com",
                           "Icon made by fps web agency from www.flaticon.com",
                           "Icon made by srip from www.flaticon.com",
                           "", "", "", "", ""]

            for index, layout in enumerate(app_credits):
                self.menu.set_label_menu_option(index, layout)

            self.menu.show()

    def default_show_widget(self, hidden_widget):
        if super(MenuManager, self).default_show_widget(hidden_widget):
            # Update Menu
            self._update_menu()

    def default_hide_widget(self, widget_to_hide):
        if super(MenuManager, self).default_hide_widget(widget_to_hide):
            if self.metric_selected >= self.shown_count:
                self.metric_selected = self.shown_count - 1

            # Update Menu
            self._update_menu()

    def menu_button_released(self):
        if self.menu_state == MenuStates.NoMenu:
            # Enter the main menu
            self.menu_state = MenuStates.InMenu

        elif self.menu_state == MenuStates.InMenu:
            # Back out of the menu
            self.menu_state = MenuStates.NoMenu

        elif self.menu_state == MenuStates.RideStats:
            # Go back to the main menu
            self.menu_state = MenuStates.InMenu

        elif self.menu_state == MenuStates.Hiding:
            # Go back to the main menu and reset the metric selected
            self.menu_state = MenuStates.InMenu
            self.metric_selected = 0

        elif self.menu_state == MenuStates.Showing:
            # Go back to the main menu and reset the metric selected
            self.menu_state = MenuStates.InMenu
            self.show_hidden_selected = 0

        elif self.menu_state == MenuStates.Moving:
            # Go back to the main menu and reset the metric selected
            self.menu_state = MenuStates.InMenu
            self.metric_selected = 0

        elif self.menu_state == MenuStates.MovingSelected:
            # Go back to the moving screen
            self.menu_state = MenuStates.Moving

        elif self.menu_state == MenuStates.LoadingLayout:
            # Go back to the main menu
            self.menu_state = MenuStates.InMenu

        elif self.menu_state == MenuStates.SavingLayout:
            # Go back to the main menu
            self.menu_state = MenuStates.InMenu

        elif self.menu_state == MenuStates.InCredits:
            # Go back to the main menu
            self.menu_state = MenuStates.InMenu

        self._update_menu()

    def enter_button_released(self):
        if self.menu_state == MenuStates.NoMenu:
            # Do nothing
            return

        elif self.menu_state == MenuStates.InMenu:
            # Take the appropriate action based on selected option
            if self.menu_option_selected == MenuOptions.RideStats.value:
                # Update our copy of the ride statistics
                self.request_update_stats.emit()

                # Pull up the ride stats
                self.menu_state = MenuStates.RideStats

            elif self.menu_option_selected == MenuOptions.LoadLayout.value:
                # Go to the load layout menu
                self.menu_state = MenuStates.LoadingLayout

            elif self.menu_option_selected == MenuOptions.HideMetric.value:
                # Go to the hide widget screen
                self.menu_state = MenuStates.Hiding

            elif self.menu_option_selected == MenuOptions.ShowMetric.value:
                # Go to the show widget screen if at least 1 widget is hidden and not in default layout
                if self.shown_count < len(self.widgets) and not self.default_layout:
                    self.menu_state = MenuStates.Showing
                else:
                    return

            elif self.menu_option_selected == MenuOptions.MoveMetric.value:
                # Go to the move screen if more than one widget is showing
                if self.shown_count > 1:
                    self.menu_state = MenuStates.Moving
                else:
                    return

            elif self.menu_option_selected == MenuOptions.SaveLayout.value:
                # Go to the save layout menu
                self.menu_state = MenuStates.SavingLayout

            elif self.menu_option_selected == MenuOptions.StartupLayout.value:
                # Change the startup layout and save it
                self.start_up_layout = (self.start_up_layout + 1) % len(StoredLayouts)
                self._save_startup_layout()

            elif self.menu_option_selected == MenuOptions.LayoutPreference.value:
                # Change the layout type (either zero or one)
                self.layout_type = (self.layout_type + 1) % 2

                # Update widgets in the background
                self._update_widgets()

            elif self.menu_option_selected == MenuOptions.Credits.value:
                # Go to the credits menu
                self.menu_state = MenuStates.InCredits

        elif self.menu_state == MenuStates.RideStats:
            # Do nothing
            return

        elif self.menu_state == MenuStates.Hiding:
            # Hide the selected widget
            self._hide_widget(self.metric_selected)

            # If the last widget was hidden update the selected metric to the new last widget
            if self.metric_selected == self.shown_count:
                self.metric_selected -= 1

            self._update_widgets()

        elif self.menu_state == MenuStates.Showing:
            # Show the selected hidden widget and exit
            self._show_widget(self.show_hidden_selected)
            self.show_hidden_selected = 0
            self.menu_state = MenuStates.NoMenu
            self._update_widgets()

        elif self.menu_state == MenuStates.Moving:
            # Go to the moving selected screen if more than one metric is showing
            if self.shown_count > 1:
                self.menu_state = MenuStates.MovingSelected
            else:
                return

        elif self.menu_state == MenuStates.MovingSelected:
            # Go back to the moving screen
            self.menu_state = MenuStates.Moving

        elif self.menu_state == MenuStates.LoadingLayout:
            # Load the selected layout and exit
            self._load_layout(self.menu_load_selected)
            self.menu_state = MenuStates.NoMenu
            self._update_widgets()

        elif self.menu_state == MenuStates.SavingLayout:
            # Save the selected layout and exit
            self._save_layout(self.menu_save_selected)
            self.menu_state = MenuStates.NoMenu

        self._update_menu()

    def up_button_released(self):
        self._up_or_down_button_released(-1)

    def down_button_released(self):
        self._up_or_down_button_released(1)

    def _up_or_down_button_released(self, direction: int):
        if self.menu_state == MenuStates.NoMenu:
            # Do nothing
            return

        elif self.menu_state == MenuStates.InMenu:
            # Change the selected menu option
            self.menu_option_selected = (self.menu_option_selected + direction) % len(MenuOptions)

        elif self.menu_state == MenuStates.RideStats:
            # Scroll the ride stat menu
            self.ride_stat_selected += direction

        elif self.menu_state == MenuStates.Hiding:
            # Change the selected widget
            self.metric_selected = (self.metric_selected + direction) % self.shown_count

        elif self.menu_state == MenuStates.Showing:
            # If there are some widgets that are hidden then change the selected option
            if (len(self.widgets) - self.shown_count) > 0:
                self.show_hidden_selected = (self.show_hidden_selected + direction) % (len(self.widgets) - self.shown_count)

        elif self.menu_state == MenuStates.Moving:
            # Change the selected widget
            self.metric_selected = (self.metric_selected + direction) % self.shown_count

        elif self.menu_state == MenuStates.MovingSelected:
            # Swap the selected widget with the adjacent widget and update
            self._swap_widget_priorities(self.metric_selected, direction)
            self._update_widgets()
            self.metric_selected = (self.metric_selected + direction) % self.shown_count

        elif self.menu_state == MenuStates.LoadingLayout:
            # Change the selected menu option
            self.menu_load_selected = (self.menu_load_selected + direction) % len(StoredLayouts)

        elif self.menu_state == MenuStates.SavingLayout:
            # Change the selected menu option
            self.menu_save_selected = (self.menu_save_selected + direction) % (len(StoredLayouts) - 1)

        self._update_menu()

    def update_ride_statistics(self, stats):
        # Filter out everything but the important values
        stats.pop("Altitude", None)
        stats.pop("Latitude", None)
        stats.pop("Longitude", None)
        stats.pop("G-Force", None)
        stats.pop("Yaw", None)
        stats.pop("Roll", None)
        stats.pop("Pitch", None)
        stats.pop("Speed", None)
        stats.pop("Current Temperature", None)
        stats.pop("Current Temperature", None)

        self.ride_statistics.update(stats)
