# PyQt Packages
from PyQt5.QtCore import QObject, pyqtSignal

# For multi-threading
import threading

# For sleep
import time

# Load configuration file
import configparser


# Helper functions for generating the dictionaries that will be passed to the UI
def generate_altitude_ui_dict(data_altimeter):
    altitude_dict = {}
    valid_data = True

    # Current Altitude
    if type(data_altimeter["Data"]["Altitude"]) is str:
        altitude_dict["Altitude"] = data_altimeter["Data"]["Altitude"] + " ft"
    else:
        altitude_dict["Altitude"] = "---"
        valid_data = False

    # Altitude Change
    if type(data_altimeter["Data"]["Change in Altitude"]) is str:
        altitude_dict["Change Altitude"] = data_altimeter["Data"]["Change in Altitude"] + " ft"
    else:
        altitude_dict["Change Altitude"] = "---"
        valid_data = False

    # Max Altitude
    if type(data_altimeter["Data"]["Max Altitude"]) is str:
        altitude_dict["Max Altitude"] = data_altimeter["Data"]["Max Altitude"] + " ft"
    else:
        altitude_dict["Max Altitude"] = "---"
        valid_data = False

    # Min Altitude
    if type(data_altimeter["Data"]["Min Altitude"]) is str:
        altitude_dict["Min Altitude"] = data_altimeter["Data"]["Min Altitude"] + " ft"
    else:
        altitude_dict["Min Altitude"] = "---"
        valid_data = False

    return altitude_dict, valid_data


def generate_compass_ui_dict(data_compass):
    compass_dict = {}
    valid_data = True

    # Cardinal direction
    if type(data_compass["Data"]["Cardinal Direction"]) is str:
        compass_dict["Cardinal Direction"] = data_compass["Data"]["Cardinal Direction"]
    else:
        compass_dict["Cardinal Direction"] = "---"
        valid_data = False

    # Degree direction
    if type(data_compass["Data"]["Degree Direction"]) is str:
        compass_dict["Degree Direction"] = data_compass["Data"]["Degree Direction"] + u'\N{DEGREE SIGN}'
    else:
        compass_dict["Degree Direction"] = "---"
        valid_data = False

    return compass_dict, valid_data


def generate_distance_ui_dict(data_gps):
    distance_dict = {}
    valid_data = True

    # Distance
    if type(data_gps["Data"]["Distance Traveled"]) is str:
        distance_dict["Distance Traveled"] = data_gps["Data"]["Distance Traveled"] + " mi"
    else:
        distance_dict["Distance Traveled"] = "---"
        valid_data = False

    # Latitude
    if type(data_gps["Data"]["Latitude"]) is str:
        distance_dict["Latitude"] = data_gps["Data"]["Latitude"]
    else:
        distance_dict["Latitude"] = "---"
        valid_data = False

    # Longitude
    if type(data_gps["Data"]["Longitude"]) is str:
        distance_dict["Longitude"] = data_gps["Data"]["Longitude"]
    else:
        distance_dict["Longitude"] = "---"
        valid_data = False

    return distance_dict, valid_data


def generate_g_force_ui_dict(data_gyro_accel):
    g_force_dict = {}
    valid_data = True

    # G-Force
    if type(data_gyro_accel["Data"]["G-Force"]) is str:
        g_force_dict["G-Force"] = data_gyro_accel["Data"]["G-Force"] + " G"
    else:
        g_force_dict["G-Force"] = "---"
        valid_data = False

    # Max G-Force
    if type(data_gyro_accel["Data"]["Max G-Force"]) is str:
        g_force_dict["Max G-Force"] = data_gyro_accel["Data"]["Max G-Force"] + " G"
    else:
        g_force_dict["Max G-Force"] = "---"
        valid_data = False

    return g_force_dict, valid_data


def generate_orientation_ui_dict(data_gyro_accel):
    orientation_dict = {}
    valid_data = True

    # Yaw
    if type(data_gyro_accel["Data"]["Yaw"]) is str:
        orientation_dict["Yaw"] = data_gyro_accel["Data"]["Yaw"] + u'\N{DEGREE SIGN}'
    else:
        orientation_dict["Yaw"] = "---"
        valid_data = False

    # Pitch
    if type(data_gyro_accel["Data"]["Roll"]) is str:
        orientation_dict["Roll"] = data_gyro_accel["Data"]["Roll"] + u'\N{DEGREE SIGN}'
    else:
        orientation_dict["Roll"] = "---"
        valid_data = False

    # Roll
    if type(data_gyro_accel["Data"]["Pitch"]) is str:
        orientation_dict["Pitch"] = data_gyro_accel["Data"]["Pitch"] + u'\N{DEGREE SIGN}'
    else:
        orientation_dict["Pitch"] = "---"
        valid_data = False

    # Max degree of incline
    if type(data_gyro_accel["Data"]["Max Degree of Incline"]) is str:
        orientation_dict["Max Degree of Incline"] = data_gyro_accel["Data"]["Max Degree of Incline"] + u'\N{DEGREE SIGN}'
    else:
        orientation_dict["Max Degree of Incline"] = "---"
        valid_data = False

    # Min degree of incline
    if type(data_gyro_accel["Data"]["Min Degree of Incline"]) is str:
        orientation_dict["Min Degree of Incline"] = data_gyro_accel["Data"]["Min Degree of Incline"] + u'\N{DEGREE SIGN}'
    else:
        orientation_dict["Min Degree of Incline"] = "---"
        valid_data = False

    return orientation_dict, valid_data


def generate_speed_ui_dict(data_gps):
    speed_dict = {}
    valid_data = True

    # Speed
    if type(data_gps["Data"]["Speed"]) is str:
        speed_dict["Speed"] = data_gps["Data"]["Speed"] + " mph"
    else:
        speed_dict["Speed"] = "---"
        valid_data = False

    # Max Speed
    if type(data_gps["Data"]["Max Speed"]) is str:
        speed_dict["Max Speed"] = data_gps["Data"]["Max Speed"] + " mph"
    else:
        speed_dict["Max Speed"] = "---"
        valid_data = False

    return speed_dict, valid_data


def generate_temperature_ui_dict(data_ambient_temp):
    temperature_dict = {}
    valid_data = True

    # Temperature
    if type(data_ambient_temp["Data"]["Current Temperature"]) is str:
        temperature_dict["Current Temperature"] = data_ambient_temp["Data"]["Current Temperature"] + u'\N{DEGREE SIGN}'
    else:
        temperature_dict["Current Temperature"] = "---"
        valid_data = False

    # Max Temperature
    if type(data_ambient_temp["Data"]["High Temp"]) is str:
        temperature_dict["High Temp"] = data_ambient_temp["Data"]["High Temp"] + u'\N{DEGREE SIGN}'
    else:
        temperature_dict["High Temp"] = "---"
        valid_data = False

    # Max Temperature
    if type(data_ambient_temp["Data"]["Low Temp"]) is str:
        temperature_dict["Low Temp"] = data_ambient_temp["Data"]["Low Temp"] + u'\N{DEGREE SIGN}'
    else:
        temperature_dict["Low Temp"] = "---"
        valid_data = False

    return temperature_dict, valid_data


def generate_time_ui_dict(data_gps):
    time_dict = {}
    valid_data = True

    # Time
    if type(data_gps["Data"]["Time"]) is str:
        time_dict["Time"] = data_gps["Data"]["Time"]
    else:
        time_dict["Time"] = "---"
        valid_data = False

    # Lap time
    # TODO: Implement this
    time_dict["Lap Time"] = "---"

    return time_dict, valid_data


#  A worker class
class Worker(QObject):
    # -------- Signals -------- #
    finished = pyqtSignal()
    stop_run = pyqtSignal()
    altitude = pyqtSignal(dict, bool)
    temperature = pyqtSignal(dict, bool)
    compass = pyqtSignal(dict, bool)
    distance = pyqtSignal(dict, bool)
    g_force = pyqtSignal(dict, bool)
    orientation = pyqtSignal(dict, bool)
    speed = pyqtSignal(dict, bool)
    time = pyqtSignal(dict, bool)

    menu_released = pyqtSignal()
    enter_released = pyqtSignal()
    up_released = pyqtSignal()
    down_released = pyqtSignal()

    def __init__(self, parser: configparser):
        super(Worker, self).__init__()

        self.simulate_data = parser.getboolean('System', 'simulate_data')
        self.debug_buttons = parser.getboolean('System', 'debug_buttons')
        self.program_terminated = False
        self.stop_run.connect(self.stop)

        # -------- Setup -------- #
        if not self.simulate_data:
            # Raspberry Pi Button library
            from gpiozero import Button

            # Import Sensor classes
            from SensorModules.Altimeter import Altimeter
            from SensorModules.AmbientTemp import AmbientTemp
            from SensorModules.Compass import Compass
            from SensorModules.GPS import GPS
            from SensorModules.Gyroscope import Gyroscope

            # Setup physical buttons
            self.button_menu = Button(23)
            self.button_enter = Button(22)
            self.button_up = Button(27)
            self.button_down = Button(17)

            self.button_menu.when_released = self.menu_released.emit
            self.button_enter.when_released = self.enter_released.emit
            self.button_up.when_released = self.up_released.emit
            self.button_down.when_released = self.down_released.emit

            # Create objects for each possible sensor module
            self.sensor_altimeter = Altimeter(parser.getfloat('Altimeter', 'pressure_sea_level'),
                                              parser.getint('Altimeter', 'read_timeout_msec'),
                                              parser.getint('Altimeter', 'reconnect_timeout_msec'),
                                              parser.getboolean('Altimeter', 'debug'))

            self.sensor_ambient_temp = AmbientTemp(parser.getint('Ambient Temp', 'read_timeout_msec'),
                                                   parser.getint('Ambient Temp', 'reconnect_timeout_msec'),
                                                   parser.getboolean('Ambient Temp', 'debug'))

            self.sensor_compass = Compass(parser.getfloat('Compass', 'x_offset'),
                                          parser.getfloat('Compass', 'y_offset'),
                                          parser.getint('Compass', 'read_timeout_msec'),
                                          parser.getint('Compass', 'reconnect_timeout_msec'),
                                          parser.getboolean('Compass', 'debug'))

            self.sensor_gps = GPS(parser.getint('GPS', 'distance_traveled_num_waits'),
                                  parser.getint('GPS', 'update_retries_limit'),
                                  parser.getint('GPS', 'read_timeout_msec'),
                                  parser.getint('GPS', 'reconnect_timeout_msec'),
                                  parser.getboolean('GPS', 'debug'))

            self.sensor_gyro_accel = Gyroscope(parser.getint('Gyro/Accel', 'read_timeout_msec'),
                                               parser.getint('Gyro/Accel', 'reconnect_timeout_msec'),
                                               parser.getboolean('Gyro/Accel', 'debug'))

            self.ui_update_rate_s = parser.getint('System', 'ui_update_rate_msec') / 1000

            # Spin up threads to run each sensor state machine
            self.thread_altimeter = threading.Thread(target=self.sensor_altimeter.run)
            self.thread_ambient_temp = threading.Thread(target=self.sensor_ambient_temp.run)
            self.thread_compass = threading.Thread(target=self.sensor_compass.run)
            self.thread_gps = threading.Thread(target=self.sensor_gps.run)
            self.thread_gyro_accel = threading.Thread(target=self.sensor_gyro_accel.run)

            self.thread_altimeter.start()
            self.thread_ambient_temp.start()
            self.thread_compass.start()
            self.thread_gps.start()
            self.thread_gyro_accel.start()

    def run(self):
        if not self.simulate_data:
            # Start indefinite loop to read sensors and update the UI
            while not self.program_terminated:
                # Read data from sensors
                data_altimeter = self.sensor_altimeter.read_data()
                data_ambient_temp = self.sensor_ambient_temp.read_data()
                data_compass = self.sensor_compass.read_data()
                data_gps = self.sensor_gps.read_data()
                data_gyro_accel = self.sensor_gyro_accel.read_data()

                # Update the user interface
                altitude_dict, valid_data = generate_altitude_ui_dict(data_altimeter)
                self.altitude.emit(altitude_dict, valid_data)
                compass_dict, valid_data = generate_compass_ui_dict(data_compass)
                self.compass.emit(compass_dict, valid_data)
                distance_dict, valid_data = generate_distance_ui_dict(data_gps)
                self.distance.emit(distance_dict, valid_data)
                g_force_dict, valid_data = generate_g_force_ui_dict(data_gyro_accel)
                self.g_force.emit(g_force_dict, valid_data)
                orientation_dict, valid_data = generate_orientation_ui_dict(data_gyro_accel)
                self.orientation.emit(orientation_dict, valid_data)
                speed_dict, valid_data = generate_speed_ui_dict(data_gps)
                self.speed.emit(speed_dict, valid_data)
                temperature_dict, valid_data = generate_temperature_ui_dict(data_ambient_temp)
                self.temperature.emit(temperature_dict, valid_data)
                time_dict, valid_data = generate_time_ui_dict(data_gps)
                self.time.emit(time_dict, valid_data)

                # Sleep to ensure we update the UI at a configurable rate
                time.sleep(self.ui_update_rate_s)

            # Join all threads together at the end
            self.thread_altimeter.join()
            self.thread_ambient_temp.join()
            self.thread_compass.join()
            self.thread_gps.join()
            self.thread_gyro_accel.join()

        else:
            while not self.program_terminated:
                # Create altitude UI dict
                dict_altitude = {"Altitude": "715 ft", "Change Altitude": "+60 ft", "Max Altitude": "760 ft",
                                 "Min Altitude": "637 ft"}
                self.altitude.emit(dict_altitude, True)

                # Create compass UI dict
                dict_compass = {"Cardinal Direction": "NW", "Degree Direction": "326" + u'\N{DEGREE SIGN}'}
                self.compass.emit(dict_compass, True)

                # Create distance UI dict
                dict_distance = {"Distance Traveled": "2.45 mi", "Latitude": "39.15498", "Longitude": "-84.40170"}
                self.distance.emit(dict_distance, True)

                # Create G-Force UI dict
                dict_g_force = {"G-Force": "0.6 G", "Max G-Force": "2.3 G"}
                self.g_force.emit(dict_g_force, True)

                # Create orientation UI dict
                dict_orientation = {"Yaw": "325.14" + u'\N{DEGREE SIGN}', "Pitch": "-2.14" + u'\N{DEGREE SIGN}',
                                    "Roll": "0.12" + u'\N{DEGREE SIGN}',
                                    "Max Degree of Incline": "6.22" + u'\N{DEGREE SIGN}',
                                    "Min Degree of Incline": "-4.50" + u'\N{DEGREE SIGN}'}
                self.orientation.emit(dict_orientation, True)

                # Create speed UI dict
                dict_speed = {"Speed": "11 mph", "Max Speed": "15 mph"}
                self.speed.emit(dict_speed, True)

                # Create temperature UI dict
                dict_temperature = {"Current Temperature": "69" + u'\N{DEGREE SIGN}',
                                    "High Temp": "71" + u'\N{DEGREE SIGN}', "Low Temp": "66" + u'\N{DEGREE SIGN}'}
                self.temperature.emit(dict_temperature, True)

                # Create time UI dict
                dict_time = {"Time": "4:20 PM", "Lap Time": "00:01:16"}
                self.time.emit(dict_time, True)

                # Just keeping this sleep timer here so that this function isn't constantly sending the widgets data
                time.sleep(1)

        self.finished.emit()

    def get_ride_stats(self):
        if not self.simulate_data:
            # Read data from sensors
            data_altimeter = self.sensor_altimeter.read_data()
            data_ambient_temp = self.sensor_ambient_temp.read_data()
            data_gps = self.sensor_gps.read_data()
            data_gyro_accel = self.sensor_gyro_accel.read_data()

            all_stats = {}

            # Combine all dictionaries!
            altitude_dict, valid_data = generate_altitude_ui_dict(data_altimeter)
            if valid_data:
                all_stats.update(altitude_dict)

            distance_dict, valid_data = generate_distance_ui_dict(data_gps)
            if valid_data:
                all_stats.update(distance_dict)

            g_force_dict, valid_data = generate_g_force_ui_dict(data_gyro_accel)
            if valid_data:
                all_stats.update(g_force_dict)

            orientation_dict, valid_data = generate_orientation_ui_dict(data_gyro_accel)
            if valid_data:
                all_stats.update(orientation_dict)

            speed_dict, valid_data = generate_speed_ui_dict(data_gps)
            if valid_data:
                all_stats.update(speed_dict)

            temperature_dict, valid_data = generate_temperature_ui_dict(data_ambient_temp)
            if valid_data:
                all_stats.update(temperature_dict)

            return all_stats
        else:
            return {"Altitude": "715 ft", "Change Altitude": "+60 ft", "Max Altitude": "760 ft",
                    "Min Altitude": "637 ft", "Distance Traveled": "2.45 mi", "Latitude": "39.15498",
                    "Longitude": "-84.40170", "G-Force": "0.6 G", "Max G-Force": "2.3 G",
                    "Yaw": "325.14" + u'\N{DEGREE SIGN}', "Pitch": "-2.14" + u'\N{DEGREE SIGN}',
                    "Roll": "0.12" + u'\N{DEGREE SIGN}', "Max Degree of Incline": "6.22" + u'\N{DEGREE SIGN}',
                    "Min Degree of Incline": "-4.50" + u'\N{DEGREE SIGN}', "Speed": "11 mph", "Max Speed": "15 mph",
                    "Current Temperature": "69" + u'\N{DEGREE SIGN}', "High Temp": "71" + u'\N{DEGREE SIGN}',
                    "Low Temp": "66" + u'\N{DEGREE SIGN}'}

    def stop(self):
        self.program_terminated = True
