from .back_end_worker import Worker
from UserInterface.Menu.menu_manager import MenuManager
from UserInterface.Menu.widgets.debug_buttons_widget import DebugButtons
from UserInterface.Metrics.widgets.altitude_widget import AltitudeWidget
from UserInterface.Metrics.widgets.temperature_widget import TemperatureWidget
from UserInterface.Metrics.widgets.compass_widget import CompassWidget
from UserInterface.Metrics.widgets.distance_widget import DistanceWidget
from UserInterface.Metrics.widgets.g_force_widget import GForceWidget
from UserInterface.Metrics.widgets.orientation_widget import OrientationWidget
from UserInterface.Metrics.widgets.speed_widget import SpeedWidget
from UserInterface.Metrics.widgets.time_widget import TimeWidget

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QThread

import configparser


class Dialog(QtWidgets.QDialog):
    def __init__(self, parser: configparser, backend: Worker):
        super(Dialog, self).__init__()

        self.debug_buttons = parser.getboolean('System', 'debug_buttons')
        self.show_ui_fullscreen = parser.getboolean('System', 'show_ui_fullscreen')
        self.show_cursor = parser.getboolean('System', 'show_cursor')

        # -------- Front End Setup -------- #
        self.setup_dialog_ui()

        # Make custom widget objects
        self.altitude = AltitudeWidget(self)
        self.temperature = TemperatureWidget(self)
        self.compass = CompassWidget(self)
        self.distance = DistanceWidget(self)
        self.g_force = GForceWidget(self)
        self.orientation = OrientationWidget(self)
        self.speed = SpeedWidget(self)
        self.time = TimeWidget(self)

        # Make menu manager
        self.menu_manager = MenuManager(
            [self.altitude, self.temperature, self.compass, self.distance, self.g_force,
             self.orientation, self.speed, self.time]
        )
        self.menu_manager.request_update_stats.connect(self.update_ui_ride_statistics)

        # -------- Back End Setup -------- #
        # Set our worker object to the one given
        self.worker = backend
        # Create a QThread object
        self.thread = QThread()
        # Setup thread and slot connections
        self.setup_backend()

        # Add UI buttons if configured
        if self.debug_buttons:
            self.debug_buttons = DebugButtons(self)
            self.debug_buttons.move(0, 0)
            self.debug_buttons.pushButton_menu.released.connect(self.menu_manager.menu_button_released)
            self.debug_buttons.pushButton_up.released.connect(self.menu_manager.up_button_released)
            self.debug_buttons.pushButton_enter.released.connect(self.menu_manager.enter_button_released)
            self.debug_buttons.pushButton_down.released.connect(self.menu_manager.down_button_released)
            self.debug_buttons.pushButton_close.released.connect(self.worker.stop_run.emit)

    def setup_backend(self):
        # Move worker to the thread
        self.worker.moveToThread(self.thread)

        # Connect signals to slots
        self.thread.started.connect(self.worker.run)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.worker.finished.connect(self.close)

        self.worker.altitude.connect(self.altitude_signal_receiver)
        self.worker.temperature.connect(self.ambient_temp_signal_receiver)
        self.worker.compass.connect(self.direction_signal_receiver)
        self.worker.distance.connect(self.distance_signal_receiver)
        self.worker.g_force.connect(self.g_force_signal_receiver)
        self.worker.orientation.connect(self.orientation_signal_receiver)
        self.worker.speed.connect(self.speed_signal_receiver)
        self.worker.time.connect(self.time_signal_receiver)

        # Connect physical buttons to functions
        self.worker.menu_released.connect(self.menu_manager.menu_button_released)
        self.worker.enter_released.connect(self.menu_manager.enter_button_released)
        self.worker.up_released.connect(self.menu_manager.up_button_released)
        self.worker.down_released.connect(self.menu_manager.down_button_released)

    def setup_dialog_ui(self):
        if self.show_ui_fullscreen:
            # Set the UI to fullscreen mode
            # This is for use on the display unit connected to the Raspberry Pi
            self.setWindowState(QtCore.Qt.WindowState.WindowFullScreen)
        else:
            # Set the UI to 800x480 (size of the display screen)
            # This is for use on desktop displays with a higher resolution
            self.resize(800, 480)
            size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            size_policy.setHorizontalStretch(0)
            size_policy.setVerticalStretch(0)
            size_policy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
            self.setSizePolicy(size_policy)
            self.setMinimumSize(QtCore.QSize(800, 480))
            self.setMaximumSize(QtCore.QSize(800, 480))

            # This will put the display in the top left of the screen
            qr = self.frameGeometry()
            cp = QtWidgets.QDesktopWidget().availableGeometry().topLeft()
            qr.moveTopLeft(cp)
            self.move(qr.topLeft())

        if not self.show_cursor:
            # Set the cursor to blank
            self.setCursor(QtCore.Qt.BlankCursor)

        # This will hide the title bar
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)

    def altitude_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.altitude, connected)

        # If the widget isn't hidden then update its contents
        if self.altitude.get_layout_priority() < self.menu_manager.shown_count:
            self.altitude.update_data(text)

    def ambient_temp_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.temperature, connected)

        # If the widget isn't hidden then update its contents
        if self.temperature.get_layout_priority() < self.menu_manager.shown_count:
            self.temperature.update_data(text)

    def direction_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.compass, connected)

        # If the widget isn't hidden then update its contents
        if self.compass.get_layout_priority() < self.menu_manager.shown_count:
            self.compass.update_data(text)

    def distance_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.distance, connected)

        # If the widget isn't hidden then update its contents
        if self.distance.get_layout_priority() < self.menu_manager.shown_count:
            self.distance.update_data(text)

    def g_force_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.g_force, connected)

        # If the widget isn't hidden then update its contents
        if self.g_force.get_layout_priority() < self.menu_manager.shown_count:
            self.g_force.update_data(text)

    def orientation_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.orientation, connected)

        # If the widget isn't hidden then update its contents
        if self.orientation.get_layout_priority() < self.menu_manager.shown_count:
            self.orientation.update_data(text)

    def speed_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.speed, connected)

        # If the widget isn't hidden then update its contents
        if self.speed.get_layout_priority() < self.menu_manager.shown_count:
            self.speed.update_data(text)

    def time_signal_receiver(self, text, connected):
        self.hide_show_if_default(self.time, connected)

        # If the widget isn't hidden then update its contents
        if self.time.get_layout_priority() < self.menu_manager.shown_count:
            self.time.update_data(text)

    def hide_show_if_default(self, widget, connected: bool):
        if connected:
            self.menu_manager.default_show_widget(widget)
        else:
            self.menu_manager.default_hide_widget(widget)

    def update_ui_ride_statistics(self):
        self.menu_manager.update_ride_statistics(self.worker.get_ride_stats())

    def showEvent(self, *args, **kwargs):
        super(QtWidgets.QDialog, self).showEvent(*args, **kwargs)

        # Start the thread
        self.thread.start()
