import pytz
import geopy.distance
from math import sqrt
from datetime import datetime


def convert_c_to_f(temp):
    # Converts Celsius to Fahrenheit
    # (0°C × 9 / 5) + 32 = 32°F
    return (temp * 9/5) + 32


def convert_knots_to_mph(speed):
    # Converts Knots to MPH
    return speed * 1.150779


def convert_meters_to_feet(distance):
    # Converts meters to feet
    return distance * 3.280839


def convert_linear_accl_to_gforce(linear_accl):
    # Converting to G-Force
    x = linear_accl[0]
    y = linear_accl[1]
    z = linear_accl[2]
    return sqrt(x * x + y * y + z * z) / 9.81


def convert_utc_to_est(time):
    est = pytz.timezone("US/Eastern")
    utc = pytz.utc

    # Note, this format, which strips the leading zero off the hour, only works on Unix. To get it to
    # work on windows, you'd need to replace the following line with fmt = '%#I:%M %p'. More info can be found on
    # https://stackoverflow.com/questions/904928/python-strftime-date-without-leading-0
    fmt = '%-I:%M %p'

    cur_time = datetime(time.tm_year, time.tm_mon, time.tm_mday, time.tm_hour, time.tm_min, time.tm_sec, tzinfo=utc)
    return cur_time.astimezone(est).strftime(fmt)


def lat_long_distance(coords_1, coords_2):
    # Returns the distance between two lat/long coords in miles
    return geopy.distance.distance(coords_1, coords_2).miles
